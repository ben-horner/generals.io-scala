package generals.sala.library.behaviors

import generals.scala.library.HistoryBot
import generals.scala.library.behaviors.{CollapseBarriers, ExplorationLoop, GameLifecycle}
import generals.scala.library.d2.{Grid, Point}
import generals.scala.library.messages.GameStart
import generals.scala.library.messages.Terrain.Fog
import generals.scala.library.tracking.Tile.{FogObstacle, General}
import generals.scala.library.tracking.{Attack, Frame, Tile, Time}
import org.scalatest.FunSpec

import scala.collection.immutable.Queue
import scala.util.Random

/**
  * Created by ben on 6/12/17.
  */
class ExplorationLoopSpec extends FunSpec {

  describe("the ExplorationLoop") {
    class TestBot(val userId: String, val userName: String, val rng: Random) extends HistoryBot
      with GameLifecycle
      with CollapseBarriers
      with ExplorationLoop

    val testBot = new TestBot("userId", "userName", new Random(42))

    val gameStart = new GameStart(
      playerIndex = 0,
      gameType = "gameType",
      replayId = "replayId",
      chatRoom = "chatRoom",
      teamChatRoom = "teamChatRoom",
      playerToUserName = IndexedSeq("me", "them"),
      playerToTeam = IndexedSeq(0, 1))

    it("should work for out-and-backs") {
      val grid: Grid = Grid(7, 19)
      var pointToTile: Map[Point, Tile] = (for {
        row <- 0 until grid.height
        col <- 0 until grid.width
      } yield {
        grid.point(row, col) -> Tile.Fog
      }).toMap
      pointToTile = pointToTile + (grid.point(0, 0) -> General(0, 1))

      pointToTile = pointToTile + (grid.point(3, 1) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(4, 1) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(5, 1) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(6, 1) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(3, 4) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(4, 4) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(5, 4) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(6, 4) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(3, 8) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(4, 8) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(5, 8) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(6, 8) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(3, 13) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(4, 13) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(5, 13) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(6, 13) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(3, 19) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(4, 19) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(5, 19) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(6, 19) -> Tile.FogObstacle)

      val frame = Frame(
        time = Time(1, 0),
        playerToStats = Map(),
        grid = grid,
        pointToTile = pointToTile,
        completedAttack = None,
        pendingAttacks = Queue[Attack]()
      )

      testBot.onGameStart(gameStart)
      testBot.onFirstFrame(frame)
      testBot.loop(frame)
    }

    it("should work for loops squeezed apart") {
      val grid: Grid = Grid(7, 19)
      var pointToTile: Map[Point, Tile] = (for {
        row <- 0 until grid.height
        col <- 0 until grid.width
      } yield {
        grid.point(row, col) -> Tile.Fog
      }).toMap
      pointToTile = pointToTile + (grid.point(0, 0) -> General(0, 1))

      pointToTile = pointToTile + (grid.point(0, 9) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(1, 9) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(2, 9) -> Tile.FogObstacle)

      pointToTile = pointToTile + (grid.point(4, 9) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(5, 9) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(6, 9) -> Tile.FogObstacle)

      val frame = Frame(
        time = Time(1, 0),
        playerToStats = Map(),
        grid = grid,
        pointToTile = pointToTile,
        completedAttack = None,
        pendingAttacks = Queue[Attack]()
      )

      testBot.onGameStart(gameStart)
      testBot.onFirstFrame(frame)
      testBot.loop(frame)
    }

    it("should be able to start a path, even on an inward pointing corner") {
      val grid: Grid = Grid(7, 7)
      var pointToTile: Map[Point, Tile] = (for {
        row <- 0 until grid.height
        col <- 0 until grid.width
      } yield {
        grid.point(row, col) -> Tile.Fog
      }).toMap
      pointToTile = pointToTile + (grid.point(3, 3) -> General(0, 1))

      pointToTile = pointToTile + (grid.point(0, 1) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(1, 0) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(1, 1) -> Tile.FogObstacle)

      val frame = Frame(
        time = Time(1, 0),
        playerToStats = Map(),
        grid = grid,
        pointToTile = pointToTile,
        completedAttack = None,
        pendingAttacks = Queue[Attack]()
      )

      testBot.onGameStart(gameStart)
      testBot.onFirstFrame(frame)
      testBot.loop(frame)
    }

    it("should still work even if part of the inset is not reachable") {
      val grid: Grid = Grid(6, 6)
      var pointToTile: Map[Point, Tile] = (for {
        row <- 0 until grid.height
        col <- 0 until grid.width
      } yield {
        grid.point(row, col) -> Tile.Fog
      }).toMap
      pointToTile = pointToTile + (grid.point(3, 3) -> General(0, 1))

      pointToTile = pointToTile + (grid.point(0, 0) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(1, 2) -> Tile.FogObstacle)
      pointToTile = pointToTile + (grid.point(2, 1) -> Tile.FogObstacle)

      val frame = Frame(
        time = Time(1, 0),
        playerToStats = Map(),
        grid = grid,
        pointToTile = pointToTile,
        completedAttack = None,
        pendingAttacks = Queue[Attack]()
      )

      testBot.onGameStart(gameStart)
      testBot.onFirstFrame(frame)
      testBot.loop(frame)
    }

  }
}
