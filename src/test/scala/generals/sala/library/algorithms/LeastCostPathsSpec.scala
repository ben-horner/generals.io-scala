package generals.sala.library.algorithms

import generals.scala.library.algorithms.{DiEdgeSetGraph, Graph, Graphs, LeastCostPaths}
import org.scalatest.FunSpec


class LeastCostPathsSpec extends FunSpec {

  describe("The leastCostPaths algorithm") {
    it("should work on degenerate graphs") {
      var graph: Graph[Int] = new DiEdgeSetGraph(Set(), Set())
      graph = graph.addNode(0)
      var costs: Map[(Int, Int), Double] = Map()

      var lcp1 = LeastCostPaths.apply[Int](graph, costs, 0)
      assert(lcp1.forest === graph)

      graph = graph.addNode(1).addEdge((0, 1))
      costs = costs + ((0, 1) -> 1.0)
      val lcp2 = LeastCostPaths[Int](graph, costs, 0)
      assert(lcp2.forest === graph)

      val lcp3 = LeastCostPaths[Int](graph, costs, 1)
      assert(lcp3.forest === new DiEdgeSetGraph[Int](Set(1), Set()))
    }
    it("should work on complex graphs") {}
    it("should terminate appropriately") {}
  }

}
