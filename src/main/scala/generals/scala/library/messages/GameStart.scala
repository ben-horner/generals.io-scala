package generals.scala.library.messages

import org.apache.log4j.Logger
import org.json.JSONObject

import scala.collection.JavaConverters._


case class GameStart(playerIndex: Int,
                     gameType: String,
                     replayId: String,
                     chatRoom: String,
                     teamChatRoom: String,
                     playerToUserName: IndexedSeq[String],
                     playerToTeam: IndexedSeq[Int]){
  require(playerIndex >= 0)
}

object GameStart {

  private val logger: Logger = Logger.getLogger(GameStart.getClass.getName)

  def apply(json: JSONObject): GameStart = {
    require(json.keys().asScala.map(_.toString).toSet.subsetOf(
      Set("game_type", "playerIndex", "replay_id", "chat_room", "team_chat_room", "usernames", "teams")))
    val userNameJson = json.getJSONArray("usernames")
    val userNames = (0 until userNameJson.length()).map(userNameJson.getString(_)).toIndexedSeq
    val teams = if (json.has("teams")) {
      val teamJson = json.getJSONArray("teams")
      (0 until teamJson.length()).map(teamJson.getInt(_)).toIndexedSeq
    } else {
      (0 until userNames.size).toIndexedSeq // everyone is on their own team
    }
    require(userNames.size == teams.size)
    new GameStart(
      playerIndex = json.getInt("playerIndex"),
      gameType = json.getString("game_type"),
      replayId = json.getString("replay_id"),
      chatRoom = json.getString("chat_room"),
      teamChatRoom = json.optString("team_chat_room"),
      playerToUserName = userNames,
      playerToTeam = teams)
  }
}
