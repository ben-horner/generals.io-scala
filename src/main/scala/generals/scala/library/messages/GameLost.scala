package generals.scala.library.messages

import org.json.JSONObject
import scala.collection.JavaConverters._


case class GameLost(killer: Int)

object GameLost {
  def apply(json: JSONObject): GameLost = {
    require(json.keys().asScala.size == 1)
    new GameLost(json.getInt("killer"))
  }
}
