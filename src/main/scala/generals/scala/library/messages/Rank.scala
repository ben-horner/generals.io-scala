package generals.scala.library.messages

import org.json.JSONObject
import scala.collection.JavaConverters._


case class Rank(duelRank: Option[Int],
                duelRankAllTime: Option[Int],
                ffaRank: Option[Int],
                ffaRankAllTime: Option[Int])

object Rank {
  def apply(json: JSONObject): Rank = {
    require(json.keys().asScala.map(_.toString).toSet.subsetOf(Set("duel", "duel-alltime", "ffa", "ffa-alltime")), s"unexpected key(s): ${json.keys().asScala.toList}")
    val duelRank = if (json.has("duel")) Some(json.getInt("duel")) else None
    val duelRankAllTime = if (json.has("duel-alltime")) Some(json.getInt("duel-alltime")) else None
    val ffaRank = if (json.has("ffa")) Some(json.getInt("ffa")) else None
    val ffaRankAllTime = if (json.has("ffa-alltime")) Some(json.getInt("ffa-alltime")) else None
    new Rank(
      duelRank = duelRank,
      duelRankAllTime = duelRankAllTime,
      ffaRank = ffaRank,
      ffaRankAllTime = ffaRankAllTime)
  }
}
