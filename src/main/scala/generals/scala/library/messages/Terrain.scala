package generals.scala.library.messages


sealed abstract class Terrain(msgVal: Int)

object Terrain {

  case object Empty extends Terrain(-1)
  case object Mountain extends Terrain(-2)
  case object Fog extends Terrain(-3)
  case object FogObstacle extends Terrain(-4)
  case class PlayerIndex(index: Int) extends Terrain(index) {
    require(index >= 0)
  }

  def valueOf(msgVal: Int): Terrain = {
    msgVal match {
      case -1 => Empty
      case -2 => Mountain
      case -3 => Fog
      case -4 => FogObstacle
      case i => PlayerIndex(i)
    }
  }

}
