package generals.scala.library.messages

import org.json.JSONObject
import scala.collection.JavaConverters._


case class GameUpdate(turn: Int,
                      mapDiff: IndexedSeq[Int],
                      citiesDiff: IndexedSeq[Int],
                      playerScores: Seq[Score],
                      playerToGeneral: IndexedSeq[Int],
                      playerToStars: Option[IndexedSeq[Double]],
                      attackIndex: Int)

object GameUpdate {
  def apply(json: JSONObject): GameUpdate = {
    require(json.keys().asScala.map(_.toString).toSet.subsetOf(
      Set("attackIndex", "turn", "map_diff", "cities_diff", "generals", "scores", "stars")),
      "Unexpected keys: " + json.keys().asScala.toList)
    val mapDiffJson = json.getJSONArray("map_diff")
    val mapDiff = (0 until mapDiffJson.length).map(mapDiffJson.getInt(_))
    val citiesDiffJson = json.getJSONArray("cities_diff")
    val citiesDiff = (0 until citiesDiffJson.length).map(citiesDiffJson.getInt(_))
    val scoresJson = json.getJSONArray("scores")
    val scores = (0 until scoresJson.length()).map(i => Score(scoresJson.getJSONObject(i)))
    val generalsJson = json.getJSONArray("generals")
    val generals = (0 until generalsJson.length).map(generalsJson.getInt(_))
    val stars =
      if (json.has("stars")) {
        val starsJson = json.optJSONArray("stars")
        Some((0 until starsJson.length()).map(starsJson.getDouble(_)))
      } else None

    new GameUpdate(
      turn = json.getInt("turn"),
      mapDiff = mapDiff,
      citiesDiff = citiesDiff,
      playerScores = scores,
      playerToGeneral = generals,
      playerToStars = stars,
      attackIndex = json.getInt("attackIndex"))
  }
}
