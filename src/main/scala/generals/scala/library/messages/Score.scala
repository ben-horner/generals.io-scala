package generals.scala.library.messages

import org.json.JSONObject
import scala.collection.JavaConverters._

case class Score(playerIndex: Int,
                 tiles: Int,
                 total: Int,
                 dead: Boolean)

object Score {
  def apply(json: JSONObject): Score = {
    require(json.keys().asScala.map(_.toString).toSet.subsetOf(Set("i", "tiles", "total", "dead")))
    new Score(
      playerIndex = json.getInt("i"),
      tiles = json.getInt("tiles"),
      total = json.getInt("total"),
      dead = json.getBoolean("dead")
    )
  }
}
