package generals.scala.library.messages

import org.json.JSONObject
import scala.collection.JavaConverters._


case class Stars(duelStars: Option[Double],
                 duelStarsAlltime: Option[Double],
                 ffaStars: Option[Double],
                 ffaStarsAlltime: Option[Double])

object Stars {
  def apply(json: JSONObject): Stars = {
    require(json.keys().asScala.map(_.toString).toSet.subsetOf(Set("duel", "duel-alltime", "ffa", "ffa-alltime")), s"unexpected key(s): ${json.keys().asScala.toList}")
    val duelStars = if (json.has("duel")) Some(json.getDouble("duel")) else None
    val duelStarsAlltime = if (json.has("duel-alltime")) Some(json.getDouble("duel-alltime")) else None
    val ffaStars = if (json.has("ffa")) Some(json.getDouble("ffa")) else None
    val ffaStarsAlltime = if (json.has("ffa-alltime")) Some(json.getDouble("ffa-alltime")) else None
    new Stars(
      duelStars = duelStars,
      duelStarsAlltime = duelStarsAlltime,
      ffaStars = ffaStars,
      ffaStarsAlltime = ffaStarsAlltime)
  }
}
