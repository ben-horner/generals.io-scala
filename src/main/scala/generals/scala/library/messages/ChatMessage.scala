package generals.scala.library.messages

import org.json.JSONObject
import scala.collection.JavaConverters._

case class ChatMessage(playerIndex: Option[Int],
                       userName: Option[String],
                       text: String)

object ChatMessage {
  def apply(json: JSONObject): ChatMessage = {
    require(json.keys().asScala.contains("text"))
    val userName =
      if (json.has("username")) Some(json.getString("username"))
      else None
    val playerIndex =
      if (json.has("playerIndex")) Some(json.getInt("playerIndex"))
      else None
    new ChatMessage(
      playerIndex = playerIndex,
      userName = userName,
      text = json.getString("text"))
  }
}
