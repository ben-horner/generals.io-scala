package generals.scala

/**
  * Created by ben on 3/19/17.
  */
package object library {

  def consoleTimer[A](name: String, block: => A): A = {
    time{(duration: Long) =>
      println("block ["+name+"] took ["+duration+"]ms")
    }(block)
  }

  def time[A](sideEffect: Long => Unit)(block: => A): A = {
    var startOpt: Option[Long] = None
    surround{
      startOpt = Some(System.currentTimeMillis())
    }{
      val duration = System.currentTimeMillis() - startOpt.get
      sideEffect(duration)
    }{
      block
    }
  }

  def surround[A](before: => Unit)(after: => Unit)(block: => A): A = {
    before
    val result: A = block
    after
    result
  }

}
