package generals.scala.library

import generals.scala.library.behaviors.GameLifecycle
import generals.scala.library.d2.{Grid, Point}
import generals.scala.library.messages.{GameStart, GameUpdate, Score, Terrain}
import generals.scala.library.tracking._
import org.apache.log4j.Logger

import scala.collection.immutable.Queue


object FrameBot {
  private val logger = Logger.getLogger(FrameBot.getClass.getName)
}

trait FrameBot extends GameLifecycle {

  import FrameBot.logger

  private var latestFrameOpt: Option[Frame] = None
  private var visibleCitiesVar: IndexedSeq[Int] = IndexedSeq() // {city indices} must aggregate state in order to patch diffs
  private var visibleMapVar: IndexedSeq[Int] = IndexedSeq() // {width, height, armies, terrain} must aggregate state in order to patch diffs
  private var knownPlayerToStarsVar: Map[Int, Double] = Map() // {player to star rating} must record, not sent every turn
  private var newAttacksSinceFrame: Queue[Attack] = Queue()
  private var attacksClearedSinceFrame: Boolean = false


  override def attack(start: Int, end: Int, is50: Boolean): Unit = {
    super.attack(start, end, is50)
    val grid = latestFrameOpt.get.grid
    newAttacksSinceFrame = newAttacksSinceFrame.enqueue(Attack(
      grid.pointForLinearIndex(start),
      grid.pointForLinearIndex(end),
      is50))
  }

  override def clearMoves(): Unit = {
    super.clearMoves()
    attacksClearedSinceFrame = true
  }

  def attack(start: Point, end: Point, is50: Boolean): Unit = {
    require(start.grid == end.grid)
    val grid = start.grid
    attack(grid.linearIndexOf(start), grid.linearIndexOf(end), is50)
  }

  def attack(start: Point, end: Point): Unit =
    attack(start, end, false)

  def onFirstFrame(frame: Frame): Unit = {}
  def onFrame(frame: Frame): Unit = {}

  override def onGameStart(gameStart: GameStart): Unit = {
    super.onGameStart(gameStart)
    latestFrameOpt = None
    visibleCitiesVar = IndexedSeq()
    visibleMapVar = IndexedSeq()
    knownPlayerToStarsVar = Map()
  }

  // TODO: onGameUpdate could probably use some helper methods
  override def onGameUpdate(gameUpdate: GameUpdate): Unit = {
    val time = Time(gameUpdate.turn, System.currentTimeMillis())
    super.onGameUpdate(gameUpdate)

    // aggregate raw-ish state from this update {map, cities, generals, scores, stars}
    visibleMapVar = patch(visibleMapVar, gameUpdate.mapDiff)
    visibleCitiesVar = patch(visibleCitiesVar, gameUpdate.citiesDiff)

    // no patch for stars, latest overwrites (may be missing in update, in which case latest stands)
    knownPlayerToStarsVar = gameUpdate.playerToStars match {
      case None => knownPlayerToStarsVar
      case Some(playerToStars) => playerToStars.indices.map(player => player -> playerToStars(player)).toMap
    }

    // break out map components {width, height, armies, terrain}
    val width = visibleMapVar(0)
    val height = visibleMapVar(1)
    val grid = if (latestFrameOpt.isDefined){
      require(latestFrameOpt.get.grid.width == width && latestFrameOpt.get.grid.height == height, s"last grid: ${latestFrameOpt.get.grid}, this: $height x $width")
      latestFrameOpt.get.grid
    } else {
      Grid(height, width)
    }

    // no patch for scores, full list comes every update
    val knownPlayerToScore: Map[Int, Score] = gameUpdate.playerScores.
      map(score => score.playerIndex -> score).toMap

    // no patch for generals, fully specified in update
    val playerToVisibleGeneral: Map[Int, Point] = gameUpdate.playerToGeneral.indices.
      filter(player => gameUpdate.playerToGeneral(player) != -1).
      map(player => player -> grid.pointForLinearIndex(gameUpdate.playerToGeneral(player))).toMap

    // build visible map, need generals and cities to do that...
    val pointToTile: Map[Point, Tile] = {
      val size = width * height
      val visibleArmies = visibleMapVar.slice(2, size + 2)
      val visibleTerrain = visibleMapVar.slice(size + 2, size + 2 + size)

      // in mapDiff, armies and terrain can change independently, making it difficult to do anything other than their patch
      // (hare to make a Map[Point, Tile] full of replacement tiles)
      (for{
        row <- 0 until height
        col <- 0 until width
        point = grid.point(row, col)
        i = grid.linearIndexOf(point)
        newTile = Tile(
          visibleTerrain = Terrain.valueOf(visibleTerrain(i)),
          visibleAsCity = visibleCitiesVar.contains(i),
          visibleAsGeneral = playerToVisibleGeneral.values.exists(_ == point),
          visibleArmies = visibleArmies(i))
      } yield {
        point -> newTile
      }).toMap
    }

    val playerToStats: Map[Int, PlayerStats] = {
      knownPlayerToScore.map { case (player, score) =>
        val stars = knownPlayerToStarsVar.get(player)
        player -> PlayerStats(player, score.tiles, score.total, score.dead, stars, None)
      }
    }

    // deal with attacks, queued up, cleared, ...
    val currentAttackQueue = if (attacksClearedSinceFrame || latestFrameOpt.isEmpty){
      newAttacksSinceFrame
    } else {
      latestFrameOpt.get.pendingAttacks.enqueue(newAttacksSinceFrame)
    }
    newAttacksSinceFrame = Queue()
    attacksClearedSinceFrame = false
    val (completedAttack, pendingAttacks) = currentAttackQueue.dequeueOption match {
      case None => (None, currentAttackQueue) // and currentAttackQueue == Queue()
      case Some((attack, remaining)) => (Some(attack), remaining)
    }

    val frame = Frame(time, playerToStats, grid, pointToTile, completedAttack, pendingAttacks)
    latestFrameOpt = Some(frame)
    if (frame.time.turn == 1){
      onFirstFrame(frame)
    }
    onFrame(frame)
  }

  def patch(old: IndexedSeq[Int], diff: IndexedSeq[Int]): IndexedSeq[Int] = {
    var out = IndexedSeq[Int]()
    var i = 0
    while (i < diff.length) {
      val matching = diff(i)
      out = out ++ old.slice(out.length, out.length + matching)
      i += 1
      if (i < diff.length){
        out = out ++ diff.slice(i+1, i+1+diff(i))
        i += diff(i)
      }
      i += 1
    }
    out
  }

}
