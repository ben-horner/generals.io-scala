package generals.scala.library

import java.net.URLEncoder

import generals.scala.library.messages._
import io.socket.client.{IO, Socket}
import io.socket.emitter.Emitter
import org.apache.log4j.Logger
import org.json.JSONObject

import scala.util.{Failure, Success, Try}


object SocketBot {

  private val logger: Logger = Logger.getLogger(SocketBot.getClass.getName)

}

// Going for the thinnest complete wrapper for communicating with the server
// This should let you avoid talking to sockets, and let you use scala types
// There may be some unnecessary wrapping in Java varargs with (Array(...).asInstanceOf[Array[Object]]: _*)
// I thought I would just use it everywhere to try to prevent surprise failures later.  Same for converting Int to Integer.

abstract class SocketBot {

  import SocketBot.logger

  def onConnect(): Unit = {}
  def onDisconnect(): Unit = {}
  def onGameStart(gameStart: GameStart): Unit = {}
  def onGameUpdate(gameUpdate: GameUpdate): Unit = {}
  def onGameLost(gameLost: GameLost): Unit = {}
  def onGameWon(): Unit = {}
  def onChatMessage(chatRoom: String, chatMessage: ChatMessage): Unit = {}
  def onStars(stars: Stars): Unit = {}
  def onRank(rank: Rank): Unit = {}


  def gameUrl(queueId: String): String = "http://bot.generals.io/games/" + URLEncoder.encode(queueId, "UTF-8")

  def replayUrl(replayId: String): String = "http://bot.generals.io/replays/" + URLEncoder.encode(replayId, "UTF-8")


  def connect(): Unit = {
    socket.connect()
  }

  def isConnected: Boolean = {
    socket.connected()
  }

  def disconnect(): Unit = {
    socket.disconnect()
  }

  def setUserName(userId: String, userName: String): Unit = {
    socket.emit("set_username", Array(userId, userName).asInstanceOf[Array[Object]]: _*)
  }


  def play(userId: String): Unit = { // free-for-all
    socket.emit("play", Array(userId).asInstanceOf[Array[Object]]: _*)
  }

  def joinFfaQueue(userId: String): Unit = {
    play(userId)
  }

  def join1v1Queue(userId: String): Unit = {
    socket.emit("join_1v1", Array(userId).asInstanceOf[Array[Object]]: _*)
  }

  def joinPrivate(customGameId: String, userId: String): Unit = {
    socket.emit("join_private", Array(customGameId, userId).asInstanceOf[Array[Object]]: _*)
  }


  def setCustomTeam(customGameId: String, team: Int): Unit = {
    socket.emit("set_custom_team", Array(customGameId, team.asInstanceOf[Integer]).asInstanceOf[Array[Object]]: _*)
  }

  def join2v2Team(teamId: String, userId: String): Unit = {
    socket.emit("join_team", Array(teamId, userId).asInstanceOf[Array[Object]]: _*)
  }

  def leave2v2Team(teamId: String): Unit = {
    socket.emit("leave_team", Array(teamId).asInstanceOf[Array[Object]]: _*)
  }


  def cancel(): Unit = { // leave queue
    socket.emit("cancel")
  }

  def leaveQueue(): Unit = {
    cancel()
  }


  def setForceStart(queueId: String, doForce: Boolean): Unit = {
    socket.emit("set_force_start", Array(queueId, doForce).asInstanceOf[Array[Object]]: _*)
  }

  def attack(start: Int, end: Int, is50: Boolean): Unit = {
    socket.emit("attack", Array(start.asInstanceOf[Integer], end.asInstanceOf[Integer], is50).asInstanceOf[Array[Object]]: _*)
  }

  def clearMoves(): Unit = {
    socket.emit("clear_moves")
  }

  def pingTile(index: Int): Unit = {
    socket.emit("ping_tile", Array(index.asInstanceOf[Integer]).asInstanceOf[Array[Object]]: _*)
  }

  def chatMessage(chatRoom: String, text: String): Unit = {
    socket.emit("chat_message", Array(chatRoom, text).asInstanceOf[Array[Object]]: _*)
  }

  def leaveGame(): Unit = {
    socket.emit("leave_game")
  }

  def starsAndRank(userId: String): Unit = {
    socket.emit("stars_and_rank", Array(userId).asInstanceOf[Array[Object]]: _*)
  }

  private val socket: Socket = IO.socket("http://botws.generals.io/")

  socket.on("connect", listener("connect") {
    onConnect()
  })

  socket.on("game_start", jsonJsonListener("game_start") {
    (data: JSONObject, garbage: JSONObject) => {
      require(garbage == null)
      val gameStart = GameStart(data)
      onGameStart(gameStart)
    }
  })

  socket.on("game_update", jsonJsonListener("game_update") {
    (data: JSONObject, garbage: JSONObject) => {
      require(Option(garbage).isEmpty)
      val gameUpdate = GameUpdate(data)
      onGameUpdate(gameUpdate)
    }
  })

  socket.on("game_lost", jsonJsonListener("game_lost") {
    (data: JSONObject, garbage: JSONObject) => {
      require(garbage == null)
      val gameLost = GameLost(data)
      onGameLost(gameLost)
    }
  })

  socket.on("game_won", jsonJsonListener("game_won") {
    (garbage1: JSONObject, garbage2: JSONObject) => {
      require(garbage1 == null && garbage2 == null)
      onGameWon()
    }
  })

  socket.on("chat_message", stringJsonListener("chat_message") {
    (chatRoom: String, data: JSONObject) => {
      val chatMessage = ChatMessage(data)
      onChatMessage(chatRoom.toString, chatMessage)
    }
  })

  socket.on("stars", jsonListener("stars") {
    (data: JSONObject) => {
      val stars = Stars(data)
      onStars(stars)
    }
  })

  socket.on("rank", jsonListener("rank") {
    (data: JSONObject) => {
      val rank = Rank(data)
      onRank(rank)
    }
  })

  socket.on("disconnect", stringListener("disconnect") {
    (data: String) => {
      require(data == "io client disconnect" || data == "ping timeout" || data == "transport error", s"unexpected data: $data")
      onDisconnect()
    }
  })

  private def listener(eventName: String)(f: => Unit): Emitter.Listener = new Emitter.Listener() {
    override def call(objects: AnyRef*): Unit = logExceptions {
      require(objects.isEmpty || objects.forall(Option(_).isEmpty), s"$eventName expected 0 argument but got [${objects.size}]: ${dataToString(objects)}")
      f
    }
  }

  private def stringListener(eventName: String)(f: String => Unit): Emitter.Listener = new Emitter.Listener() {
    override def call(objects: AnyRef*): Unit = logExceptions {
      require(objects.size == 1 || objects.drop(1).forall(Option(_).isEmpty), s"$eventName expected 1 argument but got [${objects.size}]: ${dataToString(objects)}")
      f(objects(0).asInstanceOf[String])
    }
  }

  private def jsonListener(eventName: String)(f: JSONObject => Unit): Emitter.Listener = new Emitter.Listener() {
    override def call(objects: AnyRef*): Unit = logExceptions {
      require(objects.size == 1 || objects.drop(1).forall(Option(_).isEmpty), s"$eventName expected 1 argument but got [${objects.size}]: ${dataToString(objects)}")
      f(objects(0).asInstanceOf[JSONObject])
    }
  }

  private def stringJsonListener(eventName: String)(f: (String, JSONObject) => Unit): Emitter.Listener = new Emitter.Listener() {
    override def call(objects: AnyRef*): Unit = logExceptions {
      require(objects.size == 2 || objects.drop(2).forall(Option(_).isEmpty), s"$eventName expected 2 argument but got [${objects.size}]: ${dataToString(objects)}")
      f(objects(0).asInstanceOf[String],
        objects(1).asInstanceOf[JSONObject])
    }
  }

  private def jsonJsonListener(eventName: String)(f: (JSONObject, JSONObject) => Unit): Emitter.Listener = new Emitter.Listener() {
    override def call(objects: AnyRef*): Unit = logExceptions {
      require(objects.size == 2 || objects.drop(2).forall(Option(_).isEmpty), s"$eventName expected 2 argument but got [${objects.size}]: ${dataToString(objects)}")
      f(objects(0).asInstanceOf[JSONObject],
        objects(1).asInstanceOf[JSONObject])
    }
  }

  private def dataToString(data: Seq[AnyRef]): String = {
    data.map { datum =>
      if (datum == null) "null"
      else datum.toString + ": " + datum.getClass.getName
    }.mkString(", ")
  }

  private def logExceptions[A](block: => A): A = {
    val t = Try(block)
    t match {
      case s: Success[A] => s
      case f: Failure[A] => {
        logger.error("an exception was thrown", f.exception)
        f
      }
    }
    t.get
  }

}
