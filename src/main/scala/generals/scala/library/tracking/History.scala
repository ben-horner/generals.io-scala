package generals.scala.library.tracking

import generals.scala.library.d2.Point
import generals.scala.library.tracking.Tile.General

import scala.collection.immutable.{Queue, SortedMap}


case class History(myIndex: Int,
                   playerToHistory: Map[Int, PlayerHistory],
                   mapHistory: MapHistory,
                   attackHistory: SortedMap[Int, Attack],
                   pendingAttackHistory: SortedMap[Int, Queue[Attack]],
                   turnToStamp: SortedMap[Int, Long]){



  def recordFrame(frame: Frame): History = {
    require(turnToStamp.lastOption.map(frame.time.turn > _._1) != Some(false)) // None for empty, Some(true) for legit

    val newPlayerToHistory = frame.playerToStats.map{ case (playerIndex, player) =>
      playerIndex -> (playerToHistory(playerIndex).recordView(frame.time.turn, player))
    }
    val newMapHistory = mapHistory.recordView(frame.time.turn, frame.pointToTile)
    val newAttackHistory = frame.completedAttack match {
      case None => attackHistory
      case Some(attack) => attackHistory + (frame.time.turn -> attack)
    }
    val newPendingAttackHistory = pendingAttackHistory + (frame.time.turn -> frame.pendingAttacks)
    val newTurnToStamp = turnToStamp + (frame.time.turn -> frame.time.stamp)

    History(myIndex,
      newPlayerToHistory,
      newMapHistory,
      newAttackHistory,
      newPendingAttackHistory,
      newTurnToStamp)
  }

  lazy val playerToGeneralDiscovered: Map[Int, (Point, Int)] =
    for {
      (point, pointHistory) <- mapHistory.pointToHistory
      SeenTile(seenTurn, tile) = pointHistory.snapShot()
      if (tile.isInstanceOf[General])
      firstSeenTurn = pointHistory.turnToTile.dropWhile(!_._2.isInstanceOf[General]).head._1
    } yield {
      val general = tile.asInstanceOf[General]
      general.playerIndex -> (point, firstSeenTurn)
    }

}
