package generals.scala.library.tracking

import generals.scala.library.d2.Point

case class Attack(start: Point, end: Point, is50: Boolean)