package generals.scala.library.tracking

import generals.scala.library.messages.Terrain


sealed trait Tile

object Tile {

  abstract class ArmyTile extends Tile {
    def armySize: Int
  }
  trait CityTile extends ArmyTile
  abstract class PlayerArmyTile extends ArmyTile {
    def playerIndex: Int
    def armySize: Int
  }

  case object Fog extends Tile
  case object FogObstacle extends Tile
  case object Mountain extends Tile
  case object Empty extends Tile
  case class UnoccupiedCity(armySize: Int) extends CityTile
  case class Army(playerIndex: Int, armySize: Int) extends PlayerArmyTile
  case class OccupiedCity(playerIndex: Int, armySize: Int) extends PlayerArmyTile with CityTile
  case class General(playerIndex: Int, armySize: Int) extends PlayerArmyTile

  def apply(visibleTerrain: Terrain,
            visibleAsCity: Boolean,
            visibleAsGeneral: Boolean,
            visibleArmies: Int): Tile = {
    (visibleTerrain, visibleAsCity, visibleAsGeneral, visibleArmies) match {
      case (Terrain.Fog, false, false, 0) => Fog
      case (Terrain.FogObstacle, false, false, 0) => FogObstacle
      case (Terrain.Mountain, false, false, 0) => Mountain
      case (Terrain.Empty, false, false, 0) => Empty
      case (Terrain.Empty, true, false, armySize) => UnoccupiedCity(armySize)
      case (Terrain.PlayerIndex(index), false, false, armySize) => Army(index, armySize)
      case (Terrain.PlayerIndex(index), true, false, armySize) => OccupiedCity(index, armySize)
      case (Terrain.PlayerIndex(index), false, true, armySize) => General(index, armySize)
      case _ => throw new IllegalArgumentException(s"Cannot determine tile from terrain: $visibleTerrain; visibleAsCity: $visibleAsCity; visibleAsGeneral: $visibleAsGeneral; visibleArmies: $visibleArmies")
    }
  }
}
