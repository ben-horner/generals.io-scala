package generals.scala.library.tracking

import generals.scala.library.d2.{Grid, Point}

import scala.collection.immutable.SortedMap


object MapHistory {

  def apply(grid: Grid): MapHistory = {
    val pointToEmptyHistory = (for {
      row <- 0 until grid.height
      col <- 0 until grid.width
    } yield {
      grid.point(row, col) -> PointHistory()
    }).toMap
    new MapHistory(grid, pointToEmptyHistory)
  }

}

case class MapHistory(grid: Grid, pointToHistory: Map[Point, PointHistory]) {

  def recordView(turn: Int, pointToTile: Map[Point, Tile]): MapHistory = {
    val newPointToHistory = pointToTile.map{ case (point, tile) =>
      if (pointToHistory.contains(point)){
        point -> pointToHistory(point).recordView(turn, tile)
      } else {
        require(turn == 1)
        point -> PointHistory(SortedMap(turn -> tile))
      }
    }
    MapHistory(grid, newPointToHistory)
  }

  def snapShot(turn: Int): Map[Point, SeenTile] =
    pointToHistory.map { case (point, pointHistory) =>
      point -> pointHistory.snapShot(turn)
    }

}