package generals.scala.library.tracking


case class Time(turn: Int, stamp: Long) extends Ordered[Time] {

  override def compare(that: Time): Int =
    this.turn.compare(that.turn)

}
