package generals.scala.library.tracking


case class SeenTile(seenTurn: Int, tile: Tile)
