package generals.scala.library.tracking

import generals.scala.library.d2.{Grid, Point}
import generals.scala.library.tracking.Tile.{CityTile, General}

import scala.collection.immutable.Queue


case class Frame(time: Time,
                 playerToStats: Map[Int, PlayerStats],
                 grid: Grid,
                 pointToTile: Map[Point, Tile],
                 completedAttack: Option[Attack],
                 pendingAttacks: Queue[Attack]) {

  lazy val playerToVisibleGeneral: Map[Int, Point] =
    for {
      (point, tile) <- pointToTile
      if tile.isInstanceOf[General]
      player = tile.asInstanceOf[General].playerIndex
    } yield {
      player -> point
    }

  lazy val visibleCities: Map[Point, CityTile] =
    for {
      (point, tile) <- pointToTile
      if tile.isInstanceOf[CityTile]
    } yield {
      point -> tile.asInstanceOf[CityTile]
    }

}
