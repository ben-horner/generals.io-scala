package generals.scala.library.tracking


case class PlayerStats(playerIndex: Int,
                       tiles: Int,
                       total: Int,
                       dead: Boolean,
                       stars: Option[Double],
                       rank: Option[Int]){

  require(tiles <= total)

  def liquidArmies(): Int = total - tiles // 1 must stay behind

}
