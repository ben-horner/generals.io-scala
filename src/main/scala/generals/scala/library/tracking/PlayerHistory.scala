package generals.scala.library.tracking

import scala.collection.immutable.SortedMap


object PlayerHistory {

  def apply(): PlayerHistory =
    new PlayerHistory(SortedMap())

}

case class PlayerHistory(turnToStats: SortedMap[Int, PlayerStats]){

  require(turnToStats.isEmpty || (turnToStats.map(_._2.playerIndex).toSet.size == 1))

  def recordView(turn: Int, playerStats: PlayerStats): PlayerHistory = {
    require(turn != 1 || turnToStats.isEmpty)
    PlayerHistory(turnToStats + (turn -> playerStats))
  }

}
