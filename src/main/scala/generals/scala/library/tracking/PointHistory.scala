package generals.scala.library.tracking

import scala.collection.immutable.SortedMap


object PointHistory {

  def apply(): PointHistory =
    new PointHistory(SortedMap())

}

case class PointHistory(turnToTile: SortedMap[Int, Tile]) {

  require(turnToTile.headOption.map(_._1 >= 1) != Some(false)) // None for empty, Some(true) for legit

  def recordView(turn: Int, tile: Tile): PointHistory = {
    require(turnToTile.lastOption.map(_._1 < turn) != Some(false)) // only allow adding to the end
    if (turnToTile.lastOption.map(_._2) != Option(tile)) {
      PointHistory(turnToTile + (turn -> tile))
    } else this
  }

  def snapShot(turn: Int): SeenTile = {
    turnToTile.to(turn).takeRight(2).toSeq match {
      case Seq((firstTurn, latest)) => SeenTile(0, latest)
      case Seq((priorTurn, prior), (latestTurn, latest)) => {
        if ((latest != Tile.Fog) && (latest != Tile.FogObstacle)) {
          SeenTile(0, latest)
        } else {
          require((prior != Tile.Fog) && (prior != Tile.FogObstacle))
          SeenTile(turn - (latestTurn - 1), prior)
        }
      }
    }
  }

  def snapShot(): SeenTile = {
    snapShot(turnToTile.last._1)
  }

}
