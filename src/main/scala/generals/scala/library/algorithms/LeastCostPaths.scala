package generals.scala.library.algorithms

import scala.annotation.tailrec


object LeastCostPaths {

  def apply[A](graph: Graph[A],
               edgeCosts: ((A, A)) => Double,
               source: A): LeastCostPaths[A] = {
    new LeastCostPaths[A](
      graph,
      edgeCosts,
      Set(source))
  }

  def apply[A](graph: Graph[A],
               edgeCosts: ((A, A)) => Double,
               source: A,  // single source
               terminateOnExplore: (A, Double) => Boolean): LeastCostPaths[A] = {
    new LeastCostPaths[A](
      graph,
      edgeCosts,
      Set(source),
      terminateOnExplore)
  }

  def apply[A](graph: Graph[A],
               edgeCosts: ((A, A)) => Double,
               source: A,  // single source
               terminateOnExplore: (A, Double) => Boolean,
               includeTerminationNode: Boolean): LeastCostPaths[A] = {
    new LeastCostPaths[A](
      graph,
      edgeCosts,
      Set(source),
      terminateOnExplore,
      includeTerminationNode)
  }

}

case class LeastCostPaths[A](graph: Graph[A],
                             edgeCosts: ((A, A)) => Double,
                             sources: Set[A],
                             terminateOnExplore: (A, Double) => Boolean = (node: A, distance: Double) => false,
                             includeTerminationNode: Boolean = false) {
  require(sources.subsetOf(graph.nodes), s"some sources not present in graph: ${sources -- graph.nodes}")
  // require non-infinite edgeCosts?  Is that necessesary?  (They are essentially disconnections)
  // require the graph be connected?  Is that necessary?  (Some shortest paths wouldn't exist)

  val forest: Graph[A] = extendAPath(
    dist = sources.map(source => (source -> 0.0)).toMap.withDefaultValue(Double.PositiveInfinity),
    prev = Map(),
    explored = Set(),
    frontier = sources)

  require(forest.nodes.forall(n => forest.inEdges(n).size <= 1))

  def leastCostPath(target: A): Graph[A] = {
    require(forest.nodes.contains(target))
    var current = target
    var result = forest.emptyGraph[A]().addNode(current)
    while(!sources.contains(current)){
      val edges = forest.inEdges(current)
      require(edges.size == 1)
      val edge = edges.head
      result = result.addNode(current).addEdge(edge)
      current = edge._1
    }
    result
  }

  def leastCostNodeSeq(target: A): Seq[A] = {
    require(forest.nodes.contains(target))
    var current = target
    var result = Seq[A](current)
    while(!sources.contains(current)){
      val edges = forest.inEdges(current)
      require(edges.size == 1)
      val edge = edges.head

      current = edge._1
      result = current +: result
    }
    result
  }

  @tailrec
  private def extendAPath(dist: Map[A, Double],
                           prev: Map[A, A],
                           explored: Set[A],
                           frontier: Set[A]): Graph[A] = {
    // if the graph is connected, all final dist values will be non-infinite
    // for all explored nodes, the paths in prev and distances in dist are final and minimum cost
    // for all frontier nodes, the paths in prev and distances in dist are minimal for traversing only explored nodes
    //    * there could be a shorter path to a frontier node through unexplored nodes
    //    * prev and dist values for unexplored nodes could change
    if (frontier.isEmpty) { // everything reachable has been explored
      buildGraph(explored, prev)
    } else {
      val exploring = frontier.minBy(dist)
      if (terminateOnExplore(exploring, dist(exploring))) { // special user defined termination
        if (includeTerminationNode) buildGraph(explored + exploring, prev)
        else buildGraph(explored, prev)
      } else {
        // some of the frontier may have "exploring" as their new prev
        // if this happens, it's because dist has decreased (needs updating)
        var distUpdates = Map[A, Double]()
        var prevUpdates = Map[A, A]()
        var newToFrontier = Set[A]()
        graph.outEdges(exploring).foreach{ case edge =>
          val (_, n) = edge
          if (!explored(n)){ // else nothing needs updating
            newToFrontier = newToFrontier + n
            val altDist = dist(exploring) + edgeCosts(edge)
            if (altDist < dist(n)) {
              distUpdates = distUpdates + (n -> altDist)
              prevUpdates = prevUpdates + (n -> exploring)
            }
          }
        }
        extendAPath( // recurse
          dist = dist ++ distUpdates,
          prev = prev ++ prevUpdates,
          explored = explored + exploring,
          frontier = frontier - exploring ++ newToFrontier)
      }
    }
  }

  private def buildGraph(explored: Set[A], prev: Map[A, A]): Graph[A] = {
    val edges: Set[(A, A)] = prev.toSeq.
      filter{ case (n2, n1) => explored.contains(n1) && explored.contains(n2) }.
      map{ case (n2, n1) => (n1, n2) }.toSet
    graph.emptyGraph[A]().addNodes(explored).addEdges(edges)
  }

}
