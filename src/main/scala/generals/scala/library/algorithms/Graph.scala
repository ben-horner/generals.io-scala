package generals.scala.library.algorithms

import scala.collection.immutable.Queue


trait Graph[A] { // could be implemented with edge set, matrix, or adjacency lists (or something crazier)
  def nodes: Set[A]
  def edges: Set[(A, A)]

  def outEdges(node: A): Set[(A, A)] // out edges and in edges would be equivalent in an undirected graph
  def inEdges(node: A): Set[(A, A)]

  def addNode(node: A): Graph[A]
  def addEdge(edge: (A, A)): Graph[A]

  def removeNode(node: A): Graph[A]
  def removeEdge(edge: (A, A)): Graph[A]

  def emptyGraph[B](): Graph[B]

  def addNodes(nodes: Traversable[A]): Graph[A] = {
    var result = this
    nodes.foreach{ node =>
      result = result.addNode(node)
    }
    result
  }
  def addEdges(edges: Traversable[(A, A)]): Graph[A] = {
    var result = this
    edges.foreach{ edge =>
      result = result.addEdge(edge)
    }
    result
  }

  def filterNodes(f: A => Boolean): Graph[A] = {
    var result = this
    nodes.filterNot(f).foreach{ node =>
      result = result.removeNode(node)
    }
    result
  }

  def filterEdges(f: ((A, A)) => Boolean): Graph[A] = {
    var result = this
    edges.filterNot(f).foreach{ edge =>
      result = result.removeEdge(edge)
    }
    result
  }

  def size: Int =
    nodes.size
  def neighbors(node: A): Set[A] = {
    val (firsts, seconds) = outEdges(node).partition{ case (as, bs) =>
      as == node
    }
    firsts.map(_._2) ++ seconds.map(_._1)
  }

  def containsNode(node: A): Boolean =
    nodes.contains(node)
  def containsEdge(edge: (A, A)): Boolean =
    outEdges(edge._1).contains(edge)

  def map[B](f: (A => B)): Graph[B] = {
    var result = emptyGraph[B]()
    nodes.foreach{ node =>
      result = result.addNode(f(node))
      outEdges(node).foreach{ edge =>
        result = result.addEdge((f(edge._1), f(edge._2)))
      }
    }
    result
  }

  // depth first search, this will allow the search to terminate once some condition is met (just cease iteration)
  def dfs(start: A) = new Iterator[A]() {
    var stack = List[A](start)
    var visited = Set[A](start)
    override def hasNext: Boolean =
      stack.nonEmpty
    override def next(): A = {
      val node = stack.head
      stack = stack.tail
      val unvisitedNeighbors = neighbors(node) -- visited
      stack = unvisitedNeighbors ++: stack
      visited = visited ++ unvisitedNeighbors
      node
    }
  }

  // breadth first search, this will allow the search to terminate once some condition is met (just cease iteration)
  // this adds the depth to each node, so termination can be based on depth in some way as well
  def bfs(start: A) = new Iterator[(A, Int)](){
    var queue = Queue[(A, Int)]((start, 0))
    var visited = Set[A]()
    override def hasNext: Boolean =
      queue.nonEmpty
    override def next(): (A, Int) = {
      val ((node, depth), rest) = queue.dequeue
      queue = rest
      val unvisitedNeighbors = neighbors(node) -- visited
      queue = queue.enqueue(unvisitedNeighbors.map((_, depth + 1)))
      visited = visited ++ unvisitedNeighbors
      (node, depth)
    }
  }

  def components: Set[Graph[A]] = {
    var result = Set[Graph[A]]()
    var unexplored = nodes
    while(unexplored.nonEmpty){
      val root = unexplored.head
      val componentNodes = dfs(root).toSet
      result = result + this.filterNodes(componentNodes)
    }
    result
  }

}

case class DiEdgeSetGraph[A](nodes: Set[A], edges: Set[(A, A)]) extends Graph[A] {
  override def outEdges(node: A): Set[(A, A)] =
    edges.filter(e => e._1 == node)

  override def inEdges(node: A): Set[(A, A)] =
    edges.filter(e => e._2 == node)

  override def addNode(node: A): Graph[A] =
    if (nodes.contains(node)) this
    else DiEdgeSetGraph[A](nodes + node, edges)

  override def addEdge(edge: (A, A)): Graph[A] =
    if (edges.contains(edge) || (edge._1 == edge._2)) this // no self edges (loops)
    else DiEdgeSetGraph[A](nodes, edges + edge)

  override def removeNode(node: A): Graph[A] =
    if (!nodes.contains(node)) this
    else DiEdgeSetGraph[A](nodes - node, edges.filter(e => e._1 != node && e._2 != node))

  override def removeEdge(edge: (A, A)): Graph[A] =
    if (!edges.contains(edge)) this
    else DiEdgeSetGraph(nodes, edges - edge)

  override def emptyGraph[B](): Graph[B] =
    DiEdgeSetGraph[B](Set(), Set())
}