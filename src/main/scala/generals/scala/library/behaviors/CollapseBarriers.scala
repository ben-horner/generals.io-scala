package generals.scala.library.behaviors

import generals.scala.library.FrameBot
import generals.scala.library.d2.{Grid, Point}
import generals.scala.library.messages.GameStart
import generals.scala.library.tracking.Tile._
import generals.scala.library.tracking.{Frame, Tile}
import org.apache.log4j.Logger

import scala.collection.immutable.Map


object CollapseBarriers {
  private val logger: Logger = Logger.getLogger(CollapseBarriers.getClass.getName)
}

trait CollapseBarriers extends FrameBot {

  import CollapseBarriers.logger

  private var pointToBarrierIndexVar: Map[Point, Int] = Map()
  private var indexToBarrierRegionVar: Map[Int, Set[Point]] = Map()

  def pointToBarrierIndex: Map[Point, Int] = pointToBarrierIndexVar
  def indexToBarrierRegion: Map[Int, Set[Point]] = indexToBarrierRegionVar

  override def onGameStart(gameStart: GameStart): Unit = {
    super.onGameStart(gameStart)
    pointToBarrierIndexVar = Map()
    indexToBarrierRegionVar = Map()
  }

  override def onFirstFrame(frame: Frame): Unit = {
    super.onFirstFrame(frame)
    indexToBarrierRegionVar = collapseBarriers(frame.grid, frame.pointToTile)
    pointToBarrierIndexVar = for {
      (index, barrierPoints) <- indexToBarrierRegionVar
      barrierPoint <- barrierPoints
    } yield { (barrierPoint -> index) }

//    logger.info("barriers:\n" +
//      frame.grid.toString{ (point: Point) =>
//      if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
//      else "    "
//    })
  }

  def collapseBarriers(grid: Grid, pointToTile: Map[Point, Tile]): Map[Int, Set[Point]] = {
    val obstacles = pointToTile.filter{ case (point, tile) =>
      ((tile == FogObstacle) || (tile == Mountain) || tile.isInstanceOf[CityTile])
    }.map(_._1).toSet ++ grid.outerWall
    val obstacleGraph = grid.baseGraph.filterNodes(obstacles)

    // group obstacles into barriers
    var ungrouped = obstacles
    val borderBarrier = obstacleGraph.dfs(grid.outerWall.head).toSet
    var indexToBarrierRegion = Map[Int, Set[Point]](0 -> borderBarrier)
    ungrouped = ungrouped -- borderBarrier
    while (ungrouped.nonEmpty) {
      val nextIndex = indexToBarrierRegion.size
      val newGroup = obstacleGraph.dfs(ungrouped.head).toSet
      indexToBarrierRegion = indexToBarrierRegion + (nextIndex -> newGroup)
      ungrouped = ungrouped -- newGroup
    }
    indexToBarrierRegion
  }

}
