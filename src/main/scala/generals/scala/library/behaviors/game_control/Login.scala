package generals.scala.library.behaviors.game_control

import generals.scala.library.SocketBot
import org.apache.log4j.Logger


object Login {
  private val logger = Logger.getLogger(Login.getClass.getName)
}

trait Login extends SocketBot {

  import Login.logger

  def userId: String
  def userName: String

  override def onConnect(): Unit = {
    super.onConnect()
    logger.info("Connected to server.")
    setUserName(userId, userName)
    logger.info(s"setUserName($userId, $userName)")
  }

}
