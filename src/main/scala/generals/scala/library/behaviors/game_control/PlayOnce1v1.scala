package generals.scala.library.behaviors.game_control

import org.apache.log4j.Logger


object PlayOnce1v1 {
  private val logger = Logger.getLogger(PlayOnce1v1.getClass.getName)
}

trait PlayOnce1v1 extends Join1v1 with AutoDisconnect
