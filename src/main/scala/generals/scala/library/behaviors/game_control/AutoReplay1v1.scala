package generals.scala.library.behaviors.game_control

import generals.scala.library.behaviors.GameLifecycle
import generals.scala.library.messages.{GameLost, GameStart}
import org.apache.log4j.Logger

object AutoReplay1v1 {
  private val logger = Logger.getLogger(AutoReplay1v1.getClass.getName)
}

trait AutoReplay1v1 extends Join1v1 with GameLifecycle {

  import AutoReplay1v1.logger

  override def onGameEnd(): Unit = {
    super.onGameEnd()
    leaveGame()
    println("5 second window to kill!")
    Thread.sleep(5000)
    starsAndRank(userId)
    logger.info(s"starsAndRank($userId)")
    join1v1Queue(userId)
  }

}
