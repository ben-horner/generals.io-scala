package generals.scala.library.behaviors.game_control

import org.apache.log4j.Logger

object Join1v1 {
  private val logger = Logger.getLogger(Join1v1.getClass.getName)
}

trait Join1v1 extends Login {

  import Join1v1.logger

  override def onConnect() = {
    super.onConnect()
    starsAndRank(userId)
    logger.info(s"starsAndRank($userId)")
    join1v1Queue(userId)
    logger.info(s"join1v1Queue($userId)")
  }

}
