package generals.scala.library.behaviors.game_control

import generals.scala.library.behaviors.GameLifecycle
import org.apache.log4j.Logger


object AutoDisconnect {
  private val logger: Logger = Logger.getLogger(AutoDisconnect.getClass.getName)
}

trait AutoDisconnect extends GameLifecycle {

  import AutoDisconnect.logger

  def replayUrl: String

  override def onGameEnd(): Unit = {
    super.onGameEnd()
    leaveGame()
    disconnect()
  }

}
