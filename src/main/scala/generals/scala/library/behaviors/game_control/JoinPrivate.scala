package generals.scala.library.behaviors.game_control

import java.net.URLEncoder

import generals.scala.library.SocketBot
import org.apache.log4j.Logger


object JoinPrivate {
  private val logger = Logger.getLogger(JoinPrivate.getClass.getName)
}

trait JoinPrivate extends SocketBot {

  import JoinPrivate.logger

  def userId: String
  def gameId: String

  override def onConnect(): Unit = {
    super.onConnect()
    starsAndRank(userId)
    logger.info(s"starsAndRank($userId)")
    joinPrivate(gameId, userId)
    setForceStart(gameId, true)
    logger.info("Joined private game at http://bot.generals.io/games/" + URLEncoder.encode(gameId, "UTF-8"))
  }

}
