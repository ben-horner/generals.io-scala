package generals.scala.library.behaviors.game_control

import org.apache.log4j.Logger

object JoinFfa {
  private val logger = Logger.getLogger(JoinFfa.getClass.getName)
}

trait JoinFfa extends Login {

  import JoinFfa.logger

  override def onConnect() = {
    super.onConnect()
    starsAndRank(userId)
    logger.info(s"starsAndRank($userId)")
    joinFfaQueue(userId)
    logger.info(s"joinFFAQueue($userId)")
  }

}
