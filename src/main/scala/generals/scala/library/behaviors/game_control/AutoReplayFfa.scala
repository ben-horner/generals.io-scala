package generals.scala.library.behaviors.game_control

import generals.scala.library.behaviors.GameLifecycle
import generals.scala.library.messages.{GameLost, GameStart}
import org.apache.log4j.Logger

object AutoReplayFfa {
  private val logger = Logger.getLogger(AutoReplayFfa.getClass.getName)
}

trait AutoReplayFfa extends JoinFfa with GameLifecycle {

  import AutoReplayFfa.logger

  override def onGameEnd(): Unit = {
    super.onGameEnd()
    leaveGame()
    println("5 second window to kill!")
    Thread.sleep(5000)
    starsAndRank(userId)
    logger.info(s"starsAndRank($userId)")
    joinFfaQueue(userId)
  }

}
