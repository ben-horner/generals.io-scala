package generals.scala.library.behaviors

import generals.scala.library.algorithms.{Graph, Graphs, LeastCostPaths}
import generals.scala.library.d2.{Grid, Point, Rotation}
import generals.scala.library.d2.Vector
import generals.scala.library.d2.Vector._
import generals.scala.library.d2.Rotation.{Clockwise, CounterClock}
import generals.scala.library.tracking.Tile.PlayerArmyTile
import generals.scala.library.tracking.{Frame, History, Tile}
import generals.scala.util.Element
import org.apache.log4j.Logger

import scala.util.Random


object ExplorationLoop {
  val logger: Logger = Logger.getLogger(ExplorationLoop.getClass.getName)
}

trait ExplorationLoop extends CollapseBarriers {

  import ExplorationLoop.logger

  var path: Seq[Point] = Seq()
  var lastAttackIndex: Int = 0

  def rng: Random

  override def onFrame(frame: Frame): Unit = {
    super.onFrame(frame)
    if (frame.time.turn == 1) {
//      path = loop(frame) // The NEW WAY
      simpleLoop(frame)
      path = defineLoop(frame)
      lastAttackIndex = 0
    }
    // if I don't own it, or there's only 1 army left, start over
    // if I own it, and there's more than one army, continue
    val lastAttackedTile: Tile = frame.pointToTile(path(lastAttackIndex))
    var continue = false
    if (lastAttackedTile.isInstanceOf[PlayerArmyTile]){
      val lastAttackedArmy = lastAttackedTile.asInstanceOf[PlayerArmyTile]
      continue =
        (lastAttackedArmy.playerIndex == myIndex) && // my army
          (lastAttackedArmy.armySize > 1) && // still have mobility
          (path.length > lastAttackIndex + 1) // still have a direction
    }
    if (continue){
      attack(path(lastAttackIndex), path(lastAttackIndex + 1))
      lastAttackIndex = (lastAttackIndex + 1) % path.size
    } else {
      attack(path(0), path(1))
      lastAttackIndex = 1
    }
    if (frame.time.turn % 10 == 0) print(".")
    if (frame.time.turn % 500 == 0) println
    if (frame.time.turn > 1500) leaveGame()
  }


  // border points are all points adjacent to boundary points
  // want to maintain visibility of all border points (as a hard constraint)
  // we want to maximize visibility of all points (increase visibility of points three from boundary)
  // the only points that may be travelled are one from boundary or two from boundary
  // we want to be efficient, and there may be some loops which take a long time to traverse and don't provide lots of visibility
  // we want to trim these loops
  def simpleLoop(frame: Frame): Unit = {//Seq[Point] = {
    val myGeneral: Point = frame.playerToVisibleGeneral(myIndex)

    val boundaryRegion: Set[Point] = indexToBarrierRegion(0)
    val barrierPoints: Set[Point] = (indexToBarrierRegion - 0).values.flatten.toSet

    val borderRegion: Set[Point] = boundaryRegion.flatMap(_.boxNeighbors).filter(!_.outOfBounds) -- boundaryRegion
    val insetRegionWithBarriers: Set[Point] = borderRegion.flatMap(_.boxNeighbors).filter(!_.outOfBounds) -- boundaryRegion -- borderRegion

    val generalRegionWithBarriers: Set[Point] = frame.grid.rectGraph.filterNodes(!boundaryRegion.contains(_)).dfs(myGeneral).toSet
    val traversableRegion: Set[Point] = generalRegionWithBarriers.intersect(borderRegion ++ insetRegionWithBarriers -- barrierPoints)

    // these are avoidable with respect to keeping borderRegion visible, they may be needed to avoid disconnections
    val avoidableBorderPoints = borderRegion.filter(point => point.boxNeighbors.intersect(barrierPoints).isEmpty)

    val newBoundaryRegion: Set[Point] = boundaryRegion ++ avoidableBorderPoints
    val newBorderPoints: Set[Point] = newBoundaryRegion.flatMap(_.boxNeighbors).filter(!_.outOfBounds) -- boundaryRegion -- avoidableBorderPoints
    require(newBorderPoints.intersect(barrierPoints).isEmpty)
    require(newBorderPoints.intersect(avoidableBorderPoints).isEmpty)

    val hiddenBorderPoints: Set[Point] = borderRegion -- newBorderPoints.flatMap(_.boxNeighbors)

    val newDisconnectedBorderRegions: Set[Set[Point]] = {
      val newBorderPointsGraph: Graph[Point] = frame.grid.rectGraph.filterNodes(newBorderPoints ++ hiddenBorderPoints)
      var components: Set[Set[Point]] = Set()
      var remainingNewBorderPoints = newBorderPoints ++ hiddenBorderPoints
      while (remainingNewBorderPoints.nonEmpty) {
        val component = newBorderPointsGraph.dfs(remainingNewBorderPoints.head).toSet
        components = components + component
        remainingNewBorderPoints = remainingNewBorderPoints -- component
      }
      components
    }

    logger.debug("simple loop data:")
    val pointToDisconnectedBorderRegionIndex = newDisconnectedBorderRegions.toIndexedSeq.zipWithIndex.flatMap{ case (br, i) => br.map(p => p -> (i+1)) }.toMap
    logger.debug(frame.grid.toString{ point =>
      if (pointToDisconnectedBorderRegionIndex.contains(point)) "b" + pointToDisconnectedBorderRegionIndex(point).toString
      else if (avoidableBorderPoints.contains(point)) "xxx"
      else if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
      else if (frame.playerToVisibleGeneral(myIndex) == point) " G "
      else "    "
    })





    // if we have disconnected components, reconnect them through border region
  }



  // is any border point which is not adjacent to any barrier points excludable?
  //    are there any such points which are not excludable?  yes, when they are connectors, to maintain visibility.
  //    are there any points that are excludable that are not in this category?

  // want to be able to see all points 1-from-border
  // want to travel 2-from-border as much as possible in order to see more points 3-from-border
  def loop(frame: Frame): Seq[Point] = {
    val myGeneral: Point = frame.playerToVisibleGeneral(myIndex)
    // none of these regions exclude barriers (borderRegion will contain none by nature)

    logger.debug("input map:")
    logger.debug(frame.grid.toString{ point =>
      if (frame.playerToVisibleGeneral(myIndex) == point) " G "
      else if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
      else "    "
    })

    val boundaryRegion: Set[Point] = indexToBarrierRegion(0)
    val generalRegion: Set[Point] = frame.grid.rectGraph.filterNodes(!boundaryRegion.contains(_)).dfs(myGeneral).toSet
    require(generalRegion.intersect(boundaryRegion).isEmpty)

    val barrierPoints: Set[Point] = (indexToBarrierRegion - 0).values.flatten.toSet
    logger.debug("barrier points:")
    logger.debug(frame.grid.toString{ point =>
      if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(barrierPoints.subsetOf(generalRegion))
    require(barrierPoints.intersect(boundaryRegion).isEmpty)

    val borderRegion: Set[Point] = generalRegion.intersect(boundaryRegion.flatMap(_.boxNeighbors).filter(!_.outOfBounds))
    logger.debug("border region:")
    logger.debug(frame.grid.toString{ point =>
      if (borderRegion.contains(point)) "b"
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
      else "    "
    })
    require(borderRegion.subsetOf(generalRegion))
    require(borderRegion.intersect(boundaryRegion).isEmpty)
    require(borderRegion.intersect(barrierPoints).isEmpty)

    val insetRegion: Set[Point] = (generalRegion -- borderRegion).intersect(borderRegion.flatMap(_.boxNeighbors).filter(!_.outOfBounds))
    logger.debug("inset region:")
    logger.debug(frame.grid.toString{ point =>
      if (insetRegion.contains(point)) "i"
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
      else "    "
    })
    require(insetRegion.subsetOf(generalRegion))
    require(insetRegion.intersect(borderRegion).isEmpty)

    val insetSubRegions: Set[Set[Point]] = contiguousSubRegions(insetRegion -- barrierPoints, frame.grid.rectGraph)
    logger.debug("inset sub regions")
    val pointToInsetSubRegionIndex = insetSubRegions.toIndexedSeq.zipWithIndex.flatMap{ case (sr, i) => sr.map(p => p -> (i+1)) }.toMap
    logger.debug(frame.grid.toString{ point =>
      if (pointToInsetSubRegionIndex.contains(point)) pointToInsetSubRegionIndex(point).toString
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(insetSubRegions.flatten.subsetOf(insetRegion))
    require(insetSubRegions.flatten.intersect(boundaryRegion).isEmpty)
    require(insetSubRegions.flatten.intersect(barrierPoints).isEmpty)

    val thickBorderGraph: Graph[Point] = frame.grid.rectGraph.filterNodes(insetSubRegions.flatten ++ borderRegion)
    logger.debug("thick border region:")
    logger.debug(frame.grid.toString{ point =>
      if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else if (thickBorderGraph.nodes.contains(point)) "t"
      else if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
      else "    "
    })
    require(thickBorderGraph.nodes.intersect(boundaryRegion).isEmpty)
    require(thickBorderGraph.nodes.intersect(barrierPoints).isEmpty)

    val connectableInsetSubRegions: Set[Set[Point]] = insetSubRegions.filter { insetSubRegion =>
      if (insetSubRegion.size > 1) true
      else {
        val otherSubRegions = (insetSubRegions - insetSubRegion).flatten
        thickBorderGraph.dfs(insetSubRegion.head).toSet.intersect(otherSubRegions).nonEmpty
      }
    }
    logger.debug("connectable inset sub regions")
    val pointToConnectableInsetSubRegionIndex = connectableInsetSubRegions.toIndexedSeq.zipWithIndex.flatMap{ case (sr, i) => sr.map(p => p -> (i+1)) }.toMap
    logger.debug(frame.grid.toString{ point =>
      if (pointToConnectableInsetSubRegionIndex.contains(point)) pointToConnectableInsetSubRegionIndex(point).toString
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(connectableInsetSubRegions.flatten.subsetOf(insetSubRegions.flatten))

    val connectedInsetSubRegions: Set[Point] = {
      var connected: Set[Point] = connectableInsetSubRegions.flatten
      val graph = frame.grid.rectGraph.filterNodes(connectableInsetSubRegions.flatten ++ borderRegion)
      if (connectableInsetSubRegions.size > 1) {
        connectableInsetSubRegions.foreach { insetSubRegion =>
          val otherSubRegions = (connectableInsetSubRegions - insetSubRegion).flatten
//          logger.debug("connecting inset sub regions:")
//          logger.debug(frame.grid.toString { point =>
//            if (insetSubRegion.contains(point)) "i"
//            else if (otherSubRegions.contains(point)) "o"
//            else if (connected.contains(point)) "c"
//            else if (graph.nodes.contains(point)) "--"
//            else "    "
//          })
          val connector1 = shortestPath(insetSubRegion, graph, otherSubRegions).init.tail.toSet
          connected = connected ++ connector1
          val graphWithoutConnector1 = graph.filterNodes(!connector1.contains(_))
          if (graphWithoutConnector1.dfs(insetSubRegion.head).toSet.intersect(otherSubRegions).nonEmpty) {
            val connector2 = shortestPath(insetSubRegion, graphWithoutConnector1, otherSubRegions).init.tail.toSet
            connected = connected ++ connector2
          }
        }
      }
      connected
    }
    logger.debug("connected inset sub regions:")
    logger.debug(frame.grid.toString{ point =>
      if (insetSubRegions.flatten.contains(point)) "i"
      else if (connectedInsetSubRegions.contains(point)) "c"
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(connectedInsetSubRegions.subsetOf(thickBorderGraph.nodes))

    val borderSubRegions: Set[Set[Point]] = contiguousSubRegions(borderRegion -- connectedInsetSubRegions.flatMap(_.boxNeighbors), frame.grid.rectGraph)
    logger.debug("border sub regions (not visible from connected insets)")
    val pointToBorderSubRegionIndex = borderSubRegions.toIndexedSeq.zipWithIndex.flatMap{ case (sr, i) => sr.map(p => p -> i) }.toMap
    logger.debug(frame.grid.toString{ point =>
      if (pointToBorderSubRegionIndex.contains(point)) pointToBorderSubRegionIndex(point).toString
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(borderSubRegions.flatten.subsetOf(borderRegion))

    val allConnectedSubRegions: Set[Point] = {
      var connected = connectedInsetSubRegions
      val graph = frame.grid.rectGraph.filterNodes(connectedInsetSubRegions ++ borderRegion)
      borderSubRegions.foreach { borderSubRegion =>
//        logger.debug("connecting border sub regions:")
//        logger.debug(frame.grid.toString { point =>
//          if (borderSubRegion.contains(point)) "b"
//          else if (connectedInsetSubRegions.contains(point)) "i"
//          else if (connected.contains(point)) "c"
//          else if (graph.nodes.contains(point)) "--"
//          else "    "
//        })
        val connector1 = shortestPath(borderSubRegion, graph, connectedInsetSubRegions).init.tail
        connected = connected ++ borderSubRegion ++ connector1
        val graphWithoutConnector1 = graph.filterNodes(!connector1.contains(_))
        if (graphWithoutConnector1.dfs(borderSubRegion.head).toSet.intersect(connectedInsetSubRegions).nonEmpty) {
          val connector2 = shortestPath(borderSubRegion, graphWithoutConnector1, connectedInsetSubRegions).init.tail
          connected = connected ++ connector2
        }
      }
      connected
    }
    logger.debug("all connected sub regions:")
    logger.debug(frame.grid.toString{ point =>
      if (allConnectedSubRegions.contains(point)) "c"
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(allConnectedSubRegions.subsetOf(thickBorderGraph.nodes))
    require(allConnectedSubRegions.intersect(boundaryRegion).isEmpty)
    require(allConnectedSubRegions.intersect(barrierPoints).isEmpty)

    val excludableBorderPoints = borderRegion -- allConnectedSubRegions
    logger.debug("excludable border points:")
    logger.debug(frame.grid.toString{ point =>
      if (excludableBorderPoints.contains(point)) "xxx"
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
      else "    "
    })
    require(excludableBorderPoints.subsetOf(borderRegion))
    require(excludableBorderPoints.flatMap(_.boxNeighbors).intersect(barrierPoints).isEmpty)

    val regionToTrace: Set[Point] = generalRegion -- barrierPoints -- excludableBorderPoints
    logger.debug("region to trace:")
    logger.debug(frame.grid.toString{ point =>
      if (regionToTrace.contains(point)) "r"
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(regionToTrace.intersect(barrierPoints).isEmpty)

    val newBorderRegion: Set[Point] = generalRegion.intersect((boundaryRegion ++ excludableBorderPoints).flatMap(_.boxNeighbors).filter(!_.outOfBounds) -- excludableBorderPoints)
    logger.debug("new border region:")
    logger.debug(frame.grid.toString{ point =>
      if (newBorderRegion.contains(point)) "n"
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(newBorderRegion.subsetOf(regionToTrace), s"newBorderRegion not subset of regionToTrace: ${newBorderRegion -- regionToTrace}")

    val generalToNewBorder: Seq[Point] = shortestPath(myGeneral, frame.grid.rectGraph.filterNodes(generalRegion -- barrierPoints), newBorderRegion)
    logger.debug("general to new border:")
    logger.debug(frame.grid.toString{ point =>
      if (generalToNewBorder.contains(point)) "g" + generalToNewBorder.lastIndexOf(point)
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(generalToNewBorder.toSet.subsetOf(generalRegion))
    require(generalToNewBorder.toSet.intersect(barrierPoints).isEmpty)

    val towardBoundary: Rotation = Rotation.values.toSeq(rng.nextInt(2))
    val awayFromBoundary: Rotation = Rotation.opposite(towardBoundary)

    val untrimmedLoop = traceBorder(regionToTrace, generalToNewBorder.last, towardBoundary)
    logger.debug("untrimmed loop:")
    logger.debug(frame.grid.toString{ point =>
      if (untrimmedLoop.contains(point)) "l" + untrimmedLoop.lastIndexOf(point)
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(untrimmedLoop.toSet.subsetOf(regionToTrace))

    val trimmedLoop = trimLoops(frame, untrimmedLoop, 1.0)
    logger.debug("trimmed loop:")
    logger.debug(frame.grid.toString{ point =>
      if (trimmedLoop.contains(point)) "t" + trimmedLoop.lastIndexOf(point)
      else if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else "    "
    })
    require(trimmedLoop.toSet.subsetOf(regionToTrace))

    val explorationPath = generalToNewBorder.init ++ trimmedLoop
    logger.debug("exploration path:")
    logger.debug(frame.grid.toString{ point =>
      if (frame.playerToVisibleGeneral(myIndex) == point) "G"
      else if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
      else if (explorationPath.contains(point)) "e" + explorationPath.lastIndexOf(point)
      else "    "
    })
    require(explorationPath.toSet.intersect(barrierPoints).isEmpty)

    explorationPath
  }

  def contiguousSubRegions(subRegionUnion: Set[Point], graph: Graph[Point]): Set[Set[Point]] = {
    var subRegions = Set[Set[Point]]()
    var remaining: Set[Point] = subRegionUnion
    val filteredGraph: Graph[Point] = graph.filterNodes(remaining)
    require(filteredGraph.nodes == subRegionUnion)
    while (remaining.nonEmpty) {
      val subRegion: Set[Point] = filteredGraph.dfs(remaining.head).toSet
      remaining = remaining -- subRegion
      subRegions = subRegions + subRegion
    }
    subRegions
  }

  def shortestPath(source: Point, graph: Graph[Point], targets: Set[Point]): Seq[Point] = {
    val lcp = LeastCostPaths(
      graph = graph,
      edgeCosts  = (_: (Point, Point)) => 1.0,
      source = source,
      terminateOnExplore = (p: Point, _: Double) => targets.contains(p),
      includeTerminationNode = true)
    val nearestTargets = lcp.forest.nodes.intersect(targets)
    require(nearestTargets.size == 1)
    val nearestTarget = nearestTargets.head
    lcp.leastCostNodeSeq(nearestTarget)
  }

  def shortestPath(sources: Set[Point], graph: Graph[Point], targets: Set[Point]): Seq[Point] = {
    require(sources.nonEmpty && graph.nodes.nonEmpty && targets.nonEmpty, s"invalid arguments:\n\tsources: $sources\n\ttargets: $targets\n\tgraph.nodes.size: ${graph.nodes.size}")
    val lcp = LeastCostPaths(
      graph = graph,
      edgeCosts  = (_: (Point, Point)) => 1.0,
      sources = sources,
      terminateOnExplore = (p: Point, _: Double) => targets.contains(p),
      includeTerminationNode = true)
    val nearestTargets = lcp.forest.nodes.intersect(targets)
    require(nearestTargets.size == 1, s"least cost path forest didn't contain exactly one target node: $nearestTargets")
    val nearestTarget = nearestTargets.head
    lcp.leastCostNodeSeq(nearestTarget)
  }

  // want to go around the perimeter efficiently
  // increase visibility (number of visible tiles)
  // increase detection perimeter (if they cross, we know)
  // increase likelihood of discovering enemy armies (which will be connected to enemy general)
  // by giving up some spread, extend path further then enemy will, to find them before they find us
  // by going around perimeter, come at the enemy from an angle that will not lead them to my general

  // go directly to nearest border
  // choose 50/50 to go counter/clockwise
  // keep the border on the appropriate side, and hug boundary
  // if we cross our own path, eliminate loop (for efficiency)
  // eventually consider the efficiency in terms if visible tiles (may lead to rounder corners and a shorter path)

  // first pass: hug border tightly (no buffer), don't eliminate loops
  def defineLoop(frame: Frame): Seq[Point] = {
    val myGeneral: Point = frame.playerToVisibleGeneral(myIndex)
    val rectGraph: Graph[Point] = frame.grid.rectGraph.filterEdges{ case (n1, n2) =>
      !pointToBarrierIndex.contains(n1) // barriers are a sink, you can reach them, but not continue the path
    }

    logger.debug("about to find LCP to border")
    // get to border
    val lcpGeneralToBorder: LeastCostPaths[Point] = LeastCostPaths(
      graph = rectGraph,
      edgeCosts = (_: (Point, Point)) => 1.0,
      source = myGeneral,
      terminateOnExplore = (node: Point, distance: Double) => indexToBarrierRegion(0).contains(node),
      includeTerminationNode = true)
    logger.debug("found LCP terminating border")

    val nearestBorder: Point = lcpGeneralToBorder.forest.nodes.filter(indexToBarrierRegion(0).contains(_)).head
    val pathToBorder: Seq[Point] = lcpGeneralToBorder.leastCostNodeSeq(nearestBorder).dropRight(1)
    logger.debug("found LCP to border: " + pathToBorder)


    val turnToward = if (rng.nextBoolean()) Clockwise else CounterClock
    var borderLoop = traceBorder(rectGraph.nodes -- indexToBarrierRegion(0), pathToBorder.last, turnToward)

    borderLoop = trimLoops(frame, borderLoop, 0.6)
    logger.info("\n" + pathToString(frame, pointToBarrierIndex, pathToBorder ++ borderLoop))
    pathToBorder ++ borderLoop
  }

  def traceBorder(region: Set[Point], start: Point, hugSide: Rotation): Seq[Point] = {
    require(region.contains(start))
    require((start.boxNeighbors -- region).nonEmpty) // start is on the border of the region

    val turnToward = hugSide
    val turnAway = Rotation.opposite(turnToward)

    var standing = start
    val boundaryOnHugSideOption = Vector.cardinals.dropWhile{ dir =>
      val facingRegion = region.contains(standing + dir)
      val boundaryOnHugSide = !region.contains(standing + dir.rotate(turnToward))
      !facingRegion || !boundaryOnHugSide
    }.headOption
    var facing = boundaryOnHugSideOption.getOrElse {
      Vector.cardinals.dropWhile{ dir =>
        val facingRegion = region.contains(standing + dir)
        val justTurnedCorner = !region.contains(standing + dir + dir.rotate(turnToward))
        !facingRegion || !justTurnedCorner
      }.head
    }

    // initial standing and facing are set, start tracing the loop!
    var borderLoop = Seq(standing)
    while((borderLoop.size < 3) || (borderLoop.take(2) != borderLoop.takeRight(2))) { // until we repeat a step
      facing = facing.rotate(turnToward) // either facing border, or just passed a corner
      while(!region.contains(standing + facing)) { // make sure we are turned away from border
        facing = facing.rotate(turnAway)
      }
      standing = standing + facing
      borderLoop = borderLoop :+ standing
    }
    require(borderLoop.take(2) == borderLoop.takeRight(2))
    borderLoop.dropRight(2)
  }

  def breakPathSegments(path: Seq[Point], breakOn: Set[Point]): Seq[Seq[Point]] = {
    path.sliding(2).foldLeft(Seq[Seq[Point]](Seq[Point]())) { (segments: Seq[Seq[Point]], points: Seq[Point]) =>
      val Seq(prev, next) = points
      val prevBlocked = breakOn.contains(prev)
      val nextBlocked = breakOn.contains(next)
      if (prevBlocked && !nextBlocked) {
        segments :+ Seq(next)
      } else if (!nextBlocked) {
        segments.init :+ (segments.last :+ next)
      } else {
        segments
      }
    }.dropWhile(_.isEmpty)
  }

  def trimLoops(frame: Frame, path: Seq[Point], minEfficiency: Double, alreadyVisible: Set[Point] = Set()): Seq[Point] = {
    val pathVisible = path.toSet.flatMap((p: Point) => p.boxNeighbors) -- pointToBarrierIndex.keySet
    logger.debug(s"path efficiency:\t(${pathVisible.size} / ${path.size}): ${1.0 * pathVisible.size / path.size}")

    val lcp = LeastCostPaths[Point](
      graph = frame.grid.rectGraph.filterNodes(path.contains(_)),
      edgeCosts = (e: (Point, Point)) => 1.0,
      path(0))
    val shortestPath: Seq[Point] = lcp.leastCostNodeSeq(path.last) // the shortest path cuts off all loops
    val shortestPathVisible: Set[Point] = shortestPath.toSet.flatMap((p: Point) => p.boxNeighbors) ++ shortestPath -- pointToBarrierIndex.keySet
    logger.debug(s"shortest path efficiency:\t(${shortestPathVisible.size} / ${shortestPath.size}): ${1.0 * shortestPathVisible.size / shortestPath.size}")

    if (shortestPath.size == path.size) { // base case: there are no loops
      shortestPath
    } else { // there are some loops, should they be included?
      var replace: Seq[(Range, Seq[Point])] = Seq()
      var start = path.indexWhere(!shortestPath.contains(_), 0) // start is not in shortest path, first of loop
      require(start >= 0)
      while (start != -1) {
        val end = path.indexWhere(shortestPath.contains(_), start) // end is not in loop, returns to path, treat exclusively
        val loop = path.slice(start, end)
        val trimmedLoop = removeDupeNeighbors(trimLoops(frame, loop, minEfficiency, shortestPathVisible ++ alreadyVisible))
        val newlyVisible = trimmedLoop.toSet.flatMap((p: Point) => p.boxNeighbors) ++ trimmedLoop -- shortestPathVisible -- alreadyVisible -- pointToBarrierIndex.keySet
        val efficiency = 1.0 * newlyVisible.size / (trimmedLoop.length + 1) // +1 for the step back onto the shortest path
        val replacement = if (efficiency >= minEfficiency) {
          logger.info(s"loop kept:\t\t[$start $end] (${newlyVisible.size} / ${trimmedLoop.length + 1}): $efficiency")
//          logger.info(s"loop: $trimmedLoop")
//          logger.info(stateToString(frame, pointToBarrierIndex, trimmedLoop, shortestPath, shortestPathVisible, alreadyVisible, newlyVisible))
          trimmedLoop
        } else {
          logger.info(s"loop deleted:\t[$start $end] (${newlyVisible.size} / ${trimmedLoop.length + 1}): $efficiency")
//          logger.info(s"loop: $trimmedLoop")
//          logger.info(stateToString(frame, pointToBarrierIndex, trimmedLoop, shortestPath, shortestPathVisible, alreadyVisible, newlyVisible))
          Seq[Point]()
        }
        replace = replace :+ (Range(start, end) -> replacement)
        start = path.indexWhere(!shortestPath.contains(_), end)
      }
      require(replace.nonEmpty)

      var trimmedPath: Seq[Point] = path.slice(0, replace.head._1.start)
      replace.sliding(2).foreach{ pair =>
        if (pair.size == 2) { // because Seq(x).sliding(2) == Seq(Seq(x))
          val Seq((range1, loop1), (range2, loop2)) = pair
          trimmedPath = trimmedPath ++ loop1
          trimmedPath = trimmedPath ++ path.slice(range1.end, range2.start)
        }
      }
      trimmedPath = trimmedPath ++ replace.last._2
      trimmedPath = trimmedPath ++ path.slice(replace.last._1.end, path.size)
      // remove redundant elements

      val trimmedPathVisible = trimmedPath.toSet.flatMap((p: Point) => p.boxNeighbors) -- pointToBarrierIndex.keySet
      logger.debug(s"trimmed path efficiency:\t(${trimmedPathVisible.size} / ${trimmedPath.size}): ${1.0 * trimmedPathVisible.size / trimmedPath.size}")

      removeDupeNeighbors(trimmedPath)
    }
  }

  def stateToString(frame: Frame,
                    pointToBarrierIndex: Map[Point, Int],
                    loop: Seq[Point],
                    shortestPath: Seq[Point],
                    shortestPathVisible: Set[Point],
                    alreadyVisible: Set[Point],
                    loopVisible: Set[Point]): String = {
    val pathsElt = Element{
      frame.grid.toString{ point =>
        if (frame.playerToVisibleGeneral(myIndex) == point) " G "
        else if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
        else if (loop.contains(point)) "l" + loop.lastIndexOf(point)
        else if (shortestPath.contains(point)) "s" + shortestPath.lastIndexOf(point)
        else "    "
      }
    }
    val visibleElt = Element{
      frame.grid.toString{ point =>
        if (frame.playerToVisibleGeneral(myIndex) == point) " G "
        else if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
        else if (shortestPathVisible.contains(point)) "S"
        else if (alreadyVisible.contains(point)) "V"
        else if (loopVisible.contains(point)) "L"
        else "    "
      }
    }
    pathsElt.beside(Element("    ")).beside(visibleElt).toString
  }

  def removeDupeNeighbors[A](s: Seq[A]): Seq[A] = {
    s.foldLeft(Seq[A]()){
      case (seq, elt) if (seq.isEmpty || (seq.last != elt)) => seq :+ elt
      case (seq, _) => seq
    }
  }

  def visibleNeighborhood(point: Point): Set[Point] = {
    val neighbors = for{
      x <- Seq(west, zero, east)
      y <- Seq(south, zero, north)
    } yield point + x + y
    val neighborhood = neighbors.filterNot(_.outOfBounds).toSet
    neighborhood -- pointToBarrierIndex.keySet
  }

  def newlyVisible(path: Seq[Point], prevVisible: Set[Point]): Int = {
    val visible = path.flatMap{ p =>
      visibleNeighborhood(p)
    }.toSet
    (visible -- prevVisible).size
  }

  def pathToString(frame: Frame, pointToBarrierIndex: Map[Point, Int], path: Seq[Point]): String = {
    frame.grid.toString{ point =>
      if (frame.playerToVisibleGeneral(myIndex) == point) " G "
      else if (pointToBarrierIndex.contains(point)) pointToBarrierIndex(point).toString
      else if (path.contains(point)) "p" + path.lastIndexOf(point)
      else "    "
    }
  }

}
