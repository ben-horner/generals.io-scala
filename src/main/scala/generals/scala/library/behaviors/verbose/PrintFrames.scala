package generals.scala.library.behaviors.verbose

import generals.scala.library.FrameBot
import generals.scala.library.tracking.Frame
import generals.scala.util.Element


trait PrintFrames extends FrameBot {

  override def onFrame(frame: Frame): Unit = {
    super.onFrame(frame)
    val players = frame.playerToStats.toSeq.sortBy(_._1).map(_._2)
    val playerTable = Element.divideRow(Seq(
      Element.col(Element("Index") +: players.map(p => Element(p.playerIndex.toString))),
      Element.col(Element("Stars") +: players.map(p => Element(p.stars.getOrElse(-1).toString))),
      Element.col(Element("Rank") +: players.map(p => Element(p.rank.getOrElse(-1).toString))),
      Element.col(Element("Liquid") +: players.map(p => Element(p.liquidArmies().toString))),
      Element.col(Element("Army") +: players.map(p => Element(p.total.toString))),
      Element.col(Element("Land") +: players.map(p => Element(p.tiles.toString))),
      Element.col(Element("Dead") +: players.map(p => Element(p.dead.toString)))
    ))

//    frame.pointToTile

    println("after turn " + frame.time.turn)
    println(playerTable.toString)

  }

}