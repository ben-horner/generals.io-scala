package generals.scala.library.behaviors.verbose

import generals.scala.library.d2.Point
import generals.scala.library.{HistoryBot, SocketBot}
import generals.scala.library.messages._
import generals.scala.library.tracking.{Frame, History}
import org.apache.log4j.Logger


object LogEvents {
  private val logger: Logger = Logger.getLogger(LogEvents.getClass.getName)
}

trait LogEvents extends HistoryBot {

  import LogEvents.logger

  override def onGameEnd(): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onGameEnd(...) at $start")
    super.onGameEnd()
    logger.info(s"completed onGameEnd() after ${System.currentTimeMillis() - start}ms")
  }

  override def onFirstFrame(frame: Frame): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onFirstFrame(...) at $start")
    super.onFirstFrame(frame)
    logger.info(s"completed onFirstFrame() after ${System.currentTimeMillis() - start}ms")
  }

  override def onFrame(frame: Frame): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onFrame(...) at $start")
    super.onFrame(frame)
    logger.info(s"completed onFrame() after ${System.currentTimeMillis() - start}ms")
  }

  override def onGeneralSpotted(playerToGeneral: Map[Int, Point]): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onGeneralSpotted($playerToGeneral) at $start")
    super.onGeneralSpotted(playerToGeneral)
    logger.info(s"completed onGeneralSpotted() after ${System.currentTimeMillis() - start}ms")
  }

  override def onTurn(history: History): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onTurn(...) at $start")
    super.onTurn(history)
    logger.info(s"completed onTurn() after ${System.currentTimeMillis() - start}ms")
  }

  override def onConnect(): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onConnect() at $start")
    super.onConnect()
    logger.info(s"completed onConnect() after ${System.currentTimeMillis() - start}ms")
  }

  override def onDisconnect(): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onDisconnect() at $start")
    super.onDisconnect()
    logger.info(s"completed onDisconnect() after ${System.currentTimeMillis() - start}ms")
  }

  override def onGameStart(gameStart: GameStart): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onGameStart($gameStart) at $start")
    super.onGameStart(gameStart)
    logger.info(s"completed onGameStart() after ${System.currentTimeMillis() - start}ms")
  }

  override def onGameUpdate(gameUpdate: GameUpdate): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onGameUpdate($gameUpdate) at $start")
    super.onGameUpdate(gameUpdate)
    logger.info(s"completed onGameUpdate() after ${System.currentTimeMillis() - start}ms")
  }

  override def onGameLost(gameLost: GameLost): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onGameLost($gameLost) at $start")
    super.onGameLost(gameLost)
    logger.info(s"completed onGameLost() after ${System.currentTimeMillis() - start}ms")
  }

  override def onGameWon(): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onGameWon() at $start")
    super.onGameWon()
    logger.info(s"completed onGameWon() after ${System.currentTimeMillis() - start}ms")
  }

  override def onChatMessage(chatRoom: String, chatMessage: ChatMessage): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onChatMessage($chatRoom, $chatMessage) at $start")
    super.onChatMessage(chatRoom, chatMessage)
    logger.info(s"completed onChatMessage() after ${System.currentTimeMillis() - start}ms")
  }

  override def onStars(stars: Stars): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onStars($stars) at $start")
    super.onStars(stars)
    logger.info(s"completed onStars() after ${System.currentTimeMillis() - start}ms")
  }

  override def onRank(rank: Rank): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin onRank($rank) at $start")
    super.onRank(rank)
    logger.info(s"completed onRank() after ${System.currentTimeMillis() - start}ms")
  }

}
