package generals.scala.library.behaviors.verbose

import generals.scala.library.SocketBot
import org.apache.log4j.Logger


object LogActions {
  private val logger: Logger = Logger.getLogger(LogActions.getClass.getName)
}

trait LogActions extends SocketBot {

  import LogActions.logger

  override def connect(): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin connect() at $start")
    super.connect()
    logger.info(s"completed connect() after ${System.currentTimeMillis() - start}ms");
  }

  override def disconnect(): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin disconnect() at $start")
    super.disconnect()
    logger.info(s"completed disconnect() after ${System.currentTimeMillis() - start}ms");
  }

  override def setUserName(userId: String, userName: String): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin setUserName($userId, $userName) at $start")
    super.setUserName(userId, userName)
    logger.info(s"completed setUserName() after ${System.currentTimeMillis() - start}ms");
  }

  override def play(userId: String): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin play($userId) at $start")
    super.play(userId)
    logger.info(s"completed play() after ${System.currentTimeMillis() - start}ms");
  }

  override def joinFfaQueue(userId: String): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin joinFfaQueue($userId) at $start")
    super.joinFfaQueue(userId)
    logger.info(s"completed joinFfaQueue() after ${System.currentTimeMillis() - start}ms");
  }

  override def join1v1Queue(userId: String): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin join1v1Queue($userId) at $start")
    super.join1v1Queue(userId)
    logger.info(s"completed join1v1Queue() after ${System.currentTimeMillis() - start}ms");
  }

  override def joinPrivate(customGameId: String, userId: String): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin joinPrivate($customGameId, $userId) at $start")
    super.joinPrivate(customGameId, userId)
    logger.info(s"completed joinPrivate() after ${System.currentTimeMillis() - start}ms");
  }

  override def setCustomTeam(customGameId: String, team: Int): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin setCustomTeam($customGameId, $team) at $start")
    super.setCustomTeam(customGameId, team)
    logger.info(s"completed setCustomTeam() after ${System.currentTimeMillis() - start}ms");
  }

  override def join2v2Team(teamId: String, userId: String): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin join2v2Team($teamId, $userId) at $start")
    super.join2v2Team(teamId, userId)
    logger.info(s"completed join2v2Team() after ${System.currentTimeMillis() - start}ms");
  }

  override def leave2v2Team(teamId: String): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin leave2v2Team($teamId) at $start")
    super.leave2v2Team(teamId)
    logger.info(s"completed leave2v2Team() after ${System.currentTimeMillis() - start}ms");
  }

  override def cancel(): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin cancel() at $start")
    super.cancel()
    logger.info(s"completed cancel() after ${System.currentTimeMillis() - start}ms");
  }

  override def leaveQueue(): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin leaveQueue() at $start")
    super.leaveQueue()
    logger.info(s"completed leaveQueue() after ${System.currentTimeMillis() - start}ms");
  }

  override def setForceStart(queueId: String, doForce: Boolean): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin setForceStart($queueId, $doForce) at $start")
    super.setForceStart(queueId, doForce)
    logger.info(s"completed setForceStart() after ${System.currentTimeMillis() - start}ms");
  }

  override def attack(start: Int, end: Int, is50: Boolean): Unit = {
    val strt = System.currentTimeMillis()
    logger.info(s"begin attack($start, $end, $is50) at $strt")
    super.attack(start, end, is50)
    logger.info(s"completed attack() after ${System.currentTimeMillis() - strt}ms");
  }

  override def clearMoves(): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin clearMoves() at $start")
    super.clearMoves()
    logger.info(s"completed clearMoves() after ${System.currentTimeMillis() - start}ms");
  }

  override def pingTile(index: Int): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin pingTile($index) at $start")
    super.pingTile(index)
    logger.info(s"completed pingTile() after ${System.currentTimeMillis() - start}ms");
  }

  override def chatMessage(chatRoom: String, text: String): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin chatMessage($chatRoom, $text) at $start")
    super.chatMessage(chatRoom, text)
    logger.info(s"completed chatMessage() after ${System.currentTimeMillis() - start}ms");
  }

  override def leaveGame(): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin leaveGame() at $start")
    super.leaveGame()
    logger.info(s"completed leaveGame() after ${System.currentTimeMillis() - start}ms");
  }

  override def starsAndRank(userId: String): Unit = {
    val start = System.currentTimeMillis()
    logger.info(s"begin starsAndRank($userId) at $start")
    super.starsAndRank(userId)
    logger.info(s"completed starsAndRank() after ${System.currentTimeMillis() - start}ms");
  }

}
