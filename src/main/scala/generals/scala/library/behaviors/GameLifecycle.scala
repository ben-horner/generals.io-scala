package generals.scala.library.behaviors

import generals.scala.library.SocketBot
import generals.scala.library.messages._

import org.apache.log4j.Logger


object GameLifecycle {
  private val logger: Logger = Logger.getLogger(GameLifecycle.getClass.getName)
}

trait GameLifecycle extends SocketBot {

  import GameLifecycle.logger

  def userId: String
  def userName: String

  private var gameStartOpt: Option[GameStart] = None
  private var starsOpt: Option[Stars] = None // TODO: this lives in between games, should it be here?
  private var rankOpt: Option[Rank] = None // TODO: this lives in between games, should it be here?

  def gameStart: GameStart = {
    gameStartOpt.get
  }
  def replayUrl: String = replayUrl(gameStart.replayId)
  def chatRoom: String = gameStart.chatRoom
  def teamChatRoom: String = gameStart.teamChatRoom

  def myIndex: Int = gameStart.playerIndex
  def myStars: Stars = starsOpt.get
  def myRank: Rank = rankOpt.get

  def numPlayers: Int = gameStart.playerToUserName.size
  def playerToUserName: Map[Int, String] =
    gameStart.playerToUserName.indices.map { i =>
      i -> gameStart.playerToUserName(i)
    }.toMap
  def playerToTeam: Map[Int, Int] =
    gameStart.playerToTeam.indices.map{ i =>
      i -> gameStart.playerToTeam(i)
    }.toMap

  def onGameEnd(): Unit = {
    logger.info("Replay is available at " + replayUrl)
  }

  override def onGameStart(gameStart: GameStart): Unit = {
    super.onGameStart(gameStart)
    gameStartOpt = Some(gameStart)
    logger.info("Replay will be available at " + replayUrl)
    val opponentIndices = gameStart.playerToUserName.indices.toSet - gameStart.playerIndex
    logger.info("Opponents: " + opponentIndices.map(gameStart.playerToUserName).mkString(", "))
  }

  override def onGameLost(gameLost: GameLost): Unit = {
    super.onGameLost(gameLost)
    logger.info("Game lost!  Killed by ["+gameStart.playerToUserName(gameLost.killer)+"]")
    onGameEnd()
  }

  override def onGameWon(): Unit = {
    super.onGameWon()
    val opponentIndices = gameStart.playerToUserName.indices.toSet - gameStart.playerIndex
    logger.info("Game won! Beat " + opponentIndices.map(gameStart.playerToUserName).mkString("[", ", ", "]"))
    onGameEnd()
  }

  override def onStars(stars: Stars): Unit = {
    super.onStars(stars)
    starsOpt = Some(stars)
  }

  override def onRank(rank: Rank): Unit = {
    super.onRank(rank)
    rankOpt = Some(rank)
  }

}
