package generals.scala.library.behaviors

import generals.scala.library.FrameBot
import generals.scala.library.d2.Point
import generals.scala.library.tracking.{Frame, Tile}
import generals.scala.library.d2.Vector

import scala.util.Random


trait RandomMoves extends FrameBot {

  def rng: Random

  override def onFrame(frame: Frame): Unit = {
    super.onFrame(frame)
    val myMobileArmies = frame.pointToTile.toSeq.filter{ case (point, tile) => tile match {
      case Tile.Army(myIndex, armySize) => armySize > 1
      case Tile.OccupiedCity(myIndex, armySize) => armySize > 1
      case Tile.General(myIndex, armySize) => armySize > 1
      case _ => false
    }}
    if (myMobileArmies.nonEmpty) {
      val start: Point = myMobileArmies(rng.nextInt(myMobileArmies.size))._1
      val neighbors = start.rectNeighbors.filter{ point =>
        frame.pointToTile(point) match {
          case Tile.Mountain => false
          case ct: Tile.CityTile => false
          case _ => true
        }
      }.toSeq
      if (neighbors.nonEmpty) {
        val end: Point = neighbors(rng.nextInt(neighbors.size))
        attack(start, end, false)
      }
    }
  }

}
