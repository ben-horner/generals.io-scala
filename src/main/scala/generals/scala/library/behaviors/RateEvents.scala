package generals.scala.library.behaviors

import generals.scala.library.HistoryBot
import generals.scala.library.tracking.{History, PlayerStats}
import org.apache.log4j.Logger


object RateEvents {
  private val logger: Logger = Logger.getLogger(RateEvents.getClass.getName)
}

trait RateEvents extends HistoryBot {

  import RateEvents.logger

  def onAheadTiles(aheadOfPlayers: Set[Int], playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)]): Unit = {}
  def onBehindTiles(behindPlayers: Set[Int], playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)]): Unit = {}
  def onAheadTotalArmies(aheadOfPlayers: Set[Int], playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)]): Unit = {}
  def onBehindTotalArmies(behindPlayers: Set[Int], playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)]): Unit = {}
  def onAheadLiquidArmies(aheadOfPlayers: Set[Int], playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)]): Unit = {}
  def onBehindLiquidArmies(behindPlayers: Set[Int], playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)]): Unit = {}
//  def onAheadSteadyArmyGrowth(aheadOfPlayers: Set[Int], playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)]): Unit = {}
//  def onBehindSteadyArmyGrowth(behindPlayers: Set[Int], playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)]): Unit = {}
//  def onAheadCities(aheadOfPlayers: Set[Int], playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)]) = {}
//  def onBehindCities(behindPlayers: Set[Int], playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)]) = {}

  override def onTurn(history: History): Unit = {
    super.onTurn(history)

    val playerToLatestStats: Map[Int, (PlayerStats, PlayerStats)] = history.playerToHistory.map { case (player, stats) =>
      val recentStats = stats.turnToStats.takeRight(2).toSeq
      recentStats match {
        case Seq() => {
          val noStats = PlayerStats(player, 0, 0, false, None, None)
          player -> (noStats, noStats)
        }
        case Seq((firstTurn, firstStats)) => {
          require(firstTurn == 1)
          require(firstStats.tiles == 1)
          require(firstStats.total == 1)
          player -> (firstStats, firstStats)
        }
        case Seq((priorTurn, priorStats), (latestTurn, latestStats)) => {
          require(priorTurn + 1 == latestTurn)
          // you can gain more than 1 tile in a turn by killing an enemy
          player -> (priorStats, latestStats)
        }
      }
    }

    val (myPriorStats, myLatestStats) = playerToLatestStats(myIndex)

    val aheadTiles = playerToLatestStats.filter { case (player, (theirPriorStats, theirLatestStats)) =>
      (myPriorStats.tiles <= theirPriorStats.tiles)  && (myLatestStats.tiles > theirLatestStats.tiles)
    }.keySet
    if (aheadTiles.nonEmpty) onAheadTiles(aheadTiles, playerToLatestStats)

    val behindTiles = playerToLatestStats.filter {case (player, (theirPriorStats, theirLatestStats)) =>
      (myPriorStats.tiles >= theirPriorStats.tiles) && (myLatestStats.tiles < theirLatestStats.tiles)
    }.keySet
    if (behindTiles.nonEmpty) onBehindTiles(behindTiles, playerToLatestStats)
    // done above
    val aheadTotalArmies = playerToLatestStats.filter { case (player, (theirPriorStats, theirLatestStats)) =>
      (myPriorStats.total <= theirPriorStats.total)  && (myLatestStats.total > theirLatestStats.total)
    }.keySet
    if (aheadTotalArmies.nonEmpty) onAheadTotalArmies(aheadTotalArmies, playerToLatestStats)

    val behindTotalArmies = playerToLatestStats.filter {case (player, (theirPriorStats, theirLatestStats)) =>
      (myPriorStats.total >= theirPriorStats.total) && (myLatestStats.total < theirLatestStats.total)
    }.keySet
    if (behindTotalArmies.nonEmpty) onBehindTotalArmies(behindTotalArmies, playerToLatestStats)

    val aheadLiquidArmies = playerToLatestStats.filter { case (player, (theirPriorStats, theirLatestStats)) =>
      (myPriorStats.liquidArmies() <= theirPriorStats.liquidArmies())  && (myLatestStats.liquidArmies() > theirLatestStats.liquidArmies())
    }.keySet
    if (aheadLiquidArmies.nonEmpty) onAheadLiquidArmies(aheadLiquidArmies, playerToLatestStats)

    val behindLiquidArmies = playerToLatestStats.filter {case (player, (theirPriorStats, theirLatestStats)) =>
      (myPriorStats.liquidArmies() >= theirPriorStats.liquidArmies()) && (myLatestStats.liquidArmies() < theirLatestStats.liquidArmies())
    }.keySet
    if (behindLiquidArmies.nonEmpty) onBehindLiquidArmies(behindLiquidArmies, playerToLatestStats)
  }

}
