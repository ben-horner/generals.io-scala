package generals.scala.library.behaviors

import generals.scala.library.{FrameBot, HistoryBot}
import generals.scala.library.d2.{Grid, Point}
import generals.scala.library.tracking.{Frame, History}
import generals.scala.library.d2.Vector._
import generals.scala.library.tracking.Tile.{Army, General, PlayerArmyTile}
import org.apache.log4j.Logger


object TrivialKiller {

  private val logger: Logger = Logger.getLogger(TrivialKiller.getClass.getName)

}

trait TrivialKiller extends HistoryBot with CollapseBarriers {

  import TrivialKiller.logger

  override def onFrame(frame: Frame): Unit = {
    super.onFrame(frame)
    // if an enemy general has a neighbor which is my army, and it's larger, kill the general
    val killerMoves = for{
      index <- (0 until numPlayers)
      if (index != myIndex)
      generalPointOpt = frame.playerToVisibleGeneral.get(index)
      if (generalPointOpt.isDefined)
      generalPoint = generalPointOpt.get
      generalTile = frame.pointToTile(generalPoint).asInstanceOf[General]
      neighborPoint <- generalPoint.rectNeighbors.filterNot(generalPoint.grid.outOfBounds)
      neighborTile = frame.pointToTile(neighborPoint)
      if (neighborTile.isInstanceOf[PlayerArmyTile])
      neighborArmyTile = neighborTile.asInstanceOf[PlayerArmyTile]
      if (neighborArmyTile.playerIndex == myIndex)
      if (neighborArmyTile.armySize > generalTile.armySize)
    } yield (neighborPoint, generalPoint)

    killerMoves.headOption match {
      case Some((myArmy, enemyGeneral)) => {
        clearMoves()
        attack(myArmy, enemyGeneral)
      }
      case None =>
    }
  }

  override def onGeneralSpotted(playerToGeneral: Map[Int, Point]): Unit = {
    super.onGeneralSpotted(playerToGeneral)
    logger.info(s"spotted general: $playerToGeneral")
  }

}
