package generals.scala.library

import generals.scala.library.d2.Point
import generals.scala.library.messages.GameStart
import generals.scala.library.tracking._
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.log4j.Logger

import scala.collection.immutable.SortedMap


object HistoryBot {
  private val logger: Logger = Logger.getLogger(HistoryBot.getClass.getName)
}

trait HistoryBot extends FrameBot {

  import HistoryBot.logger

  private var historyOpt: Option[History] = None

  def onTurn(history: History): Unit = {}
  def onGeneralSpotted(playerToGeneral: Map[Int, Point]): Unit = {}

  override def onGameStart(gameStart: GameStart): Unit = {
    super.onGameStart(gameStart)
    historyOpt = None
  }

  override def onFirstFrame(frame: Frame): Unit = {
    super.onFirstFrame(frame)
    historyOpt = Some(History(
      gameStart.playerIndex,
      gameStart.playerToUserName.indices.map(_ -> PlayerHistory()).toMap,
      MapHistory(frame.grid),
      SortedMap(),
      SortedMap(),
      SortedMap()))
  }

  override def onFrame(frame: Frame): Unit = {
    super.onFrame(frame)
    historyOpt = historyOpt.map(_.recordFrame(frame))
    checkForSpottedGenerals(frame)
    onTurn(historyOpt.get)
  }

  private def checkForSpottedGenerals(frame: Frame): Unit = {
    val generalDiscoveredThisTurn = for {
      (player, (point, turn)) <- historyOpt.get.playerToGeneralDiscovered
      if turn == frame.time.turn
      if player != myIndex
    } yield {
      player -> point
    }
    if (generalDiscoveredThisTurn.nonEmpty){
      onGeneralSpotted(generalDiscoveredThisTurn)
    }
  }

  override def onGameEnd(): Unit = {
    super.onGameEnd()
    val ds = new DescriptiveStatistics()
    val stamps = historyOpt.get.turnToStamp.unzip._2
    stamps.drop(1).zip(stamps).map{ case (s2, s1) => s2 - s1 }.foreach(ds.addValue(_))
    logger.info("time between turns: " + Seq(1, 25, 50, 75, 99, 100).map(p => (p + "%", ds.getPercentile(p))).mkString(","))
  }

}
