package generals.scala.library.d2


sealed trait Rotation

object Rotation {
  case object CounterClock extends Rotation
  case object Clockwise extends Rotation

  def opposite(rotation: Rotation): Rotation = rotation match {
    case CounterClock => Clockwise
    case Clockwise => CounterClock
  }

  val values = Set(CounterClock, Clockwise)
}
