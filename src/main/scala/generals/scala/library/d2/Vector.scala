package generals.scala.library.d2

import generals.scala.library.d2.Rotation._

case class Vector(dRow: Int, dCol: Int){
  def unary_- : Vector = Vector(-dRow, -dCol)
  def -(that: Vector): Vector = this + -that
  def +(that: Vector): Vector = Vector(this.dRow + that.dRow, this.dCol + that.dCol)
  def *(scalar: Int): Vector = Vector(this.dRow * scalar, this.dCol * scalar)

  def rotate(rotation: Rotation): Vector = {
    rotation match {
      case CounterClock => Vector(-dCol, dRow)
      case Clockwise => Vector(dCol, -dRow)
    }
  }

  // using manhattan distance for now, since that's how moves are made
  def length: Int = math.abs(dRow) + math.abs(dCol)
}
object Vector {
  val zero = Vector(0, 0)
  val north = Vector(-1, 0)
  val south = Vector(1, 0)
  val west = Vector(0, -1)
  val east = Vector(0, 1)
  val cardinals = Seq(north, west, south, east)
}

