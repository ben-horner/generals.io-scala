package generals.scala.library.d2

import generals.scala.library.algorithms.{DiEdgeSetGraph, Graph}
import generals.scala.library.tracking.Frame
import generals.scala.util.Element

case class Grid(height: Int, width: Int) {

  def size = height * width

  def point(vector: Vector): Point ={
    Point(vector, this)
  }

  def point(row: Int, col: Int): Point = {
    point(Vector(row, col))
  }

  def linearIndexOf(point: Point): Int = {
    require(point.grid == this)
    (point.row * width + point.col)
  }

  def pointForLinearIndex(index: Int): Point = {
    require((0 <= index) && (index < size))
    point(index / width, index % width)
  }

  def offGrid(point: Point): Boolean = {
    ((point.row < -1) || (point.row > height) || (point.col < -1) || (point.col > width))
  }

  def outOfBounds(point: Point): Boolean = {
    ((point.row < 0) || (point.row >= height) || (point.col < 0) || (point.col >= width))
  }

  val baseGraph: Graph[Point] = {
    var result: Graph[Point] = new DiEdgeSetGraph[Point](Set(), Set())
    for{
      row <- -1 to height
      col <- -1 to width
      point = this.point(row, col)
      neighbor <- point.boxNeighbors
    } {
      result = result.addNode(point).addNode(neighbor).addEdge((point, neighbor))
    }
    result
  }

  val outerWall: Set[Point] = baseGraph.nodes.filter(outOfBounds)

  val rectGraph: Graph[Point] = baseGraph.filterEdges{ case (n1, n2) =>
    (n1.row == n2.row) || (n1.col == n2.col)
  }

  def toString(pointToString: Point => String): String = {
    val colIndexRow: Traversable[Element] =
      Element(" ") +: (-1 to width).map(col => Element(col.toString)) :+ Element(" ")

    val rowsOfCells: Traversable[Traversable[Element]] =
      colIndexRow +:
      (-1 to height).map { row =>
        val rowElements = (-1 to width).map { col =>
          Element(pointToString(point(row, col)))
        }
        Element(row.toString) +: rowElements :+ Element(row.toString)
      } :+
        colIndexRow
    Element(toString()).above(
      Element.divideGrid(rowsOfCells).boxed).toString
  }

}
