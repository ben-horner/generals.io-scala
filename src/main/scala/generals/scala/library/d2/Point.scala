package generals.scala.library.d2

import generals.scala.library.d2.Vector._


case class Point private [d2] (vector: Vector, grid: Grid) {
  def row: Int = vector.dRow
  def col: Int = vector.dCol

  def +(offset: Vector): Point = grid.point(vector + offset)
  def -(offset: Vector): Point = grid.point(vector - offset)
  def -(that: Point): Vector = this.vector - that.vector

  def distance(that: Point): Int = (that - this).length

  def boxNeighbors: Set[Point] = {
    val neighbors = for{
      x <- Seq(west, zero, east)
      y <- Seq(south, zero, north)
    } yield this + x + y
    val neighborhood = neighbors.filterNot(grid.offGrid).toSet
    neighborhood - this
  }

  def rectNeighbors: Set[Point] = {
    val neighbors = Set(north, south, east, west).map(this + _)
    val neighborhood = neighbors.filterNot(grid.offGrid)
    neighborhood
  }

  def outOfBounds: Boolean =
    grid.outOfBounds(this)

  def offGrid: Boolean =
    grid.offGrid(this)

  override def toString: String = s"Point($row,$col)"

}
