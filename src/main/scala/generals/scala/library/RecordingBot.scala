package generals.scala.library

import java.net.URLEncoder

import generals.scala.library.messages.GameStart
import generals.scala.library.tracking.Frame

import scala.collection.immutable.SortedMap


class RecordingBot(val userId: String, val userName: String) extends FrameBot {

  private var myPlayerIndex: Option[Int] = None
  private var turnToFrame: SortedMap[Int, Frame] = SortedMap()

  override def onFrame(frame: Frame): Unit = {
    super.onFrame(frame)
    require(!turnToFrame.contains(frame.time.turn))
    require((frame.time.turn == 0) || turnToFrame.contains(frame.time.turn - 1))
    turnToFrame = turnToFrame + (frame.time.turn -> frame)
  }

  override def onGameStart(gameStart: GameStart): Unit = {
    super.onGameStart(gameStart)
    myPlayerIndex = Some(gameStart.playerIndex)
    turnToFrame = SortedMap()
  }

}
