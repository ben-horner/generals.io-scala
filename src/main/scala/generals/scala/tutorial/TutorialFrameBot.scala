package generals.scala.tutorial

import generals.scala.library.FrameBot
import generals.scala.library.behaviors.game_control.{AutoDisconnect, JoinPrivate}
import generals.scala.library.behaviors.RandomMoves

import scala.util.Random


object TutorialFrameBot {

  def main(args: Array[String]): Unit = {
    val rng = new Random(42)

    val userId = rng.alphanumeric.take(10).mkString("")
    val userName = rng.alphanumeric.take(10).mkString("")
    val gameId = rng.alphanumeric.take(10).mkString("")

    val bot = new TutorialFrameBot(userId, userName, gameId, rng)
    bot.connect()
  }

}


class TutorialFrameBot(val userId: String, val userName: String, val gameId: String, val rng: Random)
  extends FrameBot
    with JoinPrivate
    with RandomMoves
    with AutoDisconnect {

  // don't need anything in here in the class body, everything is handled by mixins!

}
