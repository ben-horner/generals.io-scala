package generals.scala.util

object Element {

  def main(args: Array[String]): Unit = {
    println("Hello World!")

    println(Element.divideCol(Seq(Element("xa"), Element("y"), Element("abcdef"))).boxed)
    println("Goodbye World!")
  }

  def apply(contents: Array[String]) = new Element(contents)

  def apply(chr: Char, width: Int, height: Int) = new Element(Array.fill(height)(chr.toString * width))

  def apply(line: String) = new Element(line.split('\n'))

  def row(elements: Traversable[Element]): Element = {
    if (elements.size == 1) elements.head
    else elements.reduce((a, b) => a beside b)
  }

  def divideRow(row: Traversable[Element]): Element = {
    if (row.size == 1) row.head
    else {
      val divider = Element('|', 1, row.map(_.height).max)
      row.reduce((a, b) => a beside divider beside b)
    }
  }

  def col(elements: Traversable[Element]): Element = {
    if (elements.size == 1) elements.head
    else elements.reduce((a, b) => a above b)
  }

  def divideCol(col: Traversable[Element]): Element = {
    if (col.size == 1) col.head
    else {
      val divider = Element('-', col.map(_.width).max, 1)
      col.reduce((a, b) => a above divider above b)
    }
  }

  def grid(rowsOfCells: Traversable[Traversable[Element]]): Element = {
    var cellWidth = 0
    var cellHeight = 0
    rowsOfCells.foreach { rowOfCells =>
      rowOfCells.foreach { cell =>
        cellWidth = math.max(cell.width, cellWidth)
        cellHeight = math.max(cell.height, cellHeight)
      }
    }
    col(rowsOfCells.map(rowOfCells =>
      row(rowOfCells.map(cell =>
        cell.widen(cellWidth).heighten(cellHeight)))
    ))
  }

  def divideGrid(rowsOfCells: Traversable[Traversable[Element]]): Element = {
    var cellWidth = 0
    var cellHeight = 0
    rowsOfCells.foreach { rowOfCells =>
      rowOfCells.foreach { cell =>
        cellWidth = math.max(cell.width, cellWidth)
        cellHeight = math.max(cell.height, cellHeight)
      }
    }
    divideCol(rowsOfCells.map(rowOfCells =>
      divideRow(rowOfCells.map(cell =>
        cell.widen(cellWidth).heighten(cellHeight)))
    ))
  }

}

class Element private (private val contents: Array[String]) {

  require(contents.length > 0)
  require(contents(0).length > 0)
  require(contents.forall(_.length == contents(0).length))

  def height = contents.length
  def width = if (height == 0) 0 else contents(0).length

  def widen(w: Int): Element = {
    if (w <= width) this
    else if (w - width == 1) this beside Element(' ', 1, this.height)
    else {
      val left = Element(' ', (w - width) / 2, height)
      val right = Element(' ', w - width - left.width, height)
      left beside this beside right
    }
  }

  def heighten(h: Int): Element = {
    if (h <= height) this
    else if (h - height == 1) this above Element(' ', this.width, 1)
    else {
      val top = Element(' ', width, (h - height) / 2)
      val bot = Element(' ', width, h - height - top.height)
      top above this above bot
    }
  }

  def beside(that: Element) =
    Element(
      for((line1, line2) <- this.heighten(that.height).contents zip that.heighten(this.height).contents
      ) yield line1 + line2
    )

  def above(that: Element) = new Element(this.widen(that.width).contents ++ that.widen(this.width).contents)
  def below(that: Element) = that above this

  def boxed = {
    val crnr = Element("+")
    val horz = Element("-")
    val vert = Element("|")
    val horzBrdr = crnr beside (if (width == 1) horz else List.fill(width)(horz).reduceLeft(_ beside _)) beside crnr
    val vertBrdr = if (height == 1) vert else List.fill(height)(vert).reduceLeft(_ above _)
    horzBrdr above (vertBrdr beside this beside vertBrdr) above horzBrdr
  }

  override def toString = contents.mkString("\n")

}
