package generals.scala

import generals.scala.library.HistoryBot
import generals.scala.library.behaviors._
import generals.scala.library.behaviors.game_control.{AutoReplay1v1, AutoReplayFfa, PlayOnce1v1}
import generals.scala.library.behaviors.verbose.{LogActions, LogEvents, PrintFrames}

import scala.util.Random


object Test {

  def main(args: Array[String]): Unit = {

    // last trait listed happens first, convention of calling super.on* as first statement means the first traits code runs first
    // some traits don't follow this convention, such as TimingStats which wants to time everything

    class TestBot(val userId: String, val userName: String, val rng: Random) extends HistoryBot
//      with PrintFrames
      with GameLifecycle

      with AutoReplay1v1
//      with AutoReplayFfa
//      with PlayOnce1v1

//      with RandomMoves
      with CollapseBarriers
      with ExplorationLoop
      with TrivialKiller

//      with RateEvents
//      with LogEvents
//      with LogActions

    val rng = new Random(42)
//    val rng = new Random

    val userId = "[Bot] " + rng.alphanumeric.take(10).mkString("")
    val userName = "[Bot] " + rng.alphanumeric.take(10).mkString("")

    val testBot = new TestBot(userId, userName, rng)
    testBot.connect()

  }

}
