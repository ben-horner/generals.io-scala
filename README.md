# generals.io-scala

This is the code you would end up with if you followed the tutorial for writing a bot for the game generals.io.

I saw this project porting things to java:  https://github.com/cyntran/generals.io-java

I wanted to use scala, so I went and followed the tutorial: http://dev.generals.io/api#tutorial, and mimicked the javascript as closely as I could.  JSON is not native, so there is some parsing, and the socket library is java rather than scala, so I had to use some implicit conversions to let you pass functions for the listeners.  It's not perfect, but I think it's pretty good.

I plan to wrap up the basic functionality also, so that a person could just focus on the logic if the bot if that's the part they're most interested in.


At this point, once you have the project cloned, you can run: ./gradlew clean farJar
Then java -cp build/libs/generals-0.0.1-SNAPSHOT.jar generals.scala.tutorial.Tutorial

It will print the URL to the console for you to copy to a browser, where you can start the game, and play against the bot from the tutorial (generals.scala.Tutorial is the class it's running).

If you write another main class that you'd like to run instead, search the pom file for generals.scala.tutorial.Tutorial, and replace that with your class.  Then when you run maven, it will produce a jar that will run your bot instead.
